「呀啊，居然能見到比自己還年輕的母親，時空穿梭真是恐怖呀」
「哈，哈⋯⋯」

我的腦子不怎麼跟得上，總之先整理一下情況。

我來到了我未來的家，那裡住著我的兒子和兒媳以及他們的孩子，然後我順勢被帶到餐桌前還喝起茶來。哈？

「先做下自我介紹吧。不過，媽媽說過不能提名字的」

我？啊，是這個時代的我啊。真是麻煩。

「我是你的兒子，這是我的妻子以及我們的女兒」
「就，就是說她是我孫女了？」
「是的！！！」

話說回來我那個兒子多少有點我的感覺。反正都是家人，我就不客氣了，他看上去一點也不聰明。這傢伙鼻尖上貼著創可貼。就大人會受的傷而已，稍微小孩子氣了一點。而且襯衫上的紐扣也弄錯了。我偶爾也會這樣。在一旁的兒媳是我兒子一點都配不上的美女。看上去比兒子小幾歲，感覺比兒子靠譜得多了。一眼就看出這兩人直接的關係性。脫線丈夫賢惠妻。

而旁邊的孫女還很小。看上去像幼兒園或者剛上小學的樣子。可愛得完全不像是我的隔代直系。生下來長得像媽媽這一點運氣就非常好了。不過髮色和特點有些我的感覺。可能是因為髮型和我小時候幾乎一樣才會產生這種感覺吧。這孩子可能有些緊張，低著頭。

不過不知道對方的名字，不知道怎麼稱呼才好。可知道了自己將來的孩子和將來的兒媳的名字沒問題嗎。

「對了花香，把拜託你的那個拿來吧」
「爸爸！」
「親愛的，不能把名字說出來！」
「哇，抱歉美香！啊，糟了，哇，媽媽，把剛才的忘了！」

孫子叫花香，兒媳叫美香嗎。這個兒子將我的遲鈍完美地繼承了。

「⋯⋯我叫賢。為了將我養育成賢明的孩子，媽媽為我取了個賢明的名字」

自己說出來了。和「我」原本的願望完全不一樣。應該說除了名字之外都不可靠的悲慘狀態。

「媽媽，不好意思，怎麼說呢，他就是這樣。啊，親愛的，你的扣子又搞錯了」
「哦哦，不好不好」

⋯⋯脫線兒子。還好你有個好老婆。而且美香的牙齒很不錯！很棒！可以和由加莉相媲美！

「爸爸，我拿來了」

到廚房去的小花香回來了。

「啊，好的，謝謝。交給奶奶吧」

兒子弄好扣子之後輕聲地回答道。

「奶奶，請拿著這個」

奶，奶奶⋯⋯被這麼叫多少有點不爽⋯⋯小花香給我的是果凍和勺子？

「奶奶說，小氦可能也餓了。但是昆虫用的果凍已經沒有賣了。所以留下一些比較好」
「謝，謝謝」

這孩子真能幹。這麼小的孩子就已經比父親要可靠多了。還好這方面像美香⋯⋯嗯？不，等等？

牙齒和我完全一樣！

啊啊啊啊啊啊啊啊啊啊啊！我，我居然將這種負面的遺産，這樣的十字架，給我的孫女背負了啊啊啊啊！

「小花香！」

我抱住了小花香。

「哇！怎麼了奶奶？」
「對不起！對不起！沒想到過了兩代還那麼像我！」

我淚流滿面地看了兒子一眼。牙齒還算好。也就是說，這是隔代遺傳。地雷一般的DNA居然在我可愛的孫女身上爆發了。這孩子以後也會被人成為溺灣式牙齒的少女嗎。

「放心吧小花香！我們的牙齒作為海灣來說是非常優秀的！」
「奶奶你怎麼了？好，好難受」
「媽，媽媽？冷靜點！」
「嗚⋯⋯」

沒想到正臨回去之際居然出現了今天最令我失望的事情。兒子不靠譜，孫女的牙齒，希望消失的東西被繼承了。

「⋯⋯謝謝你的果凍」

我輕撫小花香的背後將手放開了。她一臉苦惱的表情。抱歉啊。

接過那一口大小的果凍。用勺子舀起一點餵給了小氦。剩下的⋯⋯嗯，我自己吃了吧。獲得未來的卡路里。味道是我所知道的草莓味。

「奶奶，這孩子就是小氦？」
「嗯，嗯。是啊」

還沒習慣１７歲就被人叫奶奶的感覺。

「我也能餵它吃果凍嗎」
「那就拜託你了」

我將勺子給了她，小花香並沒有用什麼特殊的表情一直盯著滿嘴塞滿果凍的小氦。真是可愛得想讓人對她脖子撓癢癢。應該是第一次見到鍬形虫卻沒有害怕。⋯⋯咦？話說小花香為什麼知道小氦的名字？

「未來的我對你們說了什麼？」

從剛才開始兒子一家就好像知道我要來的樣子。似乎是未來的我告訴他們的，但是未來的我先在哪？

「那個就由我來說明吧，媽媽」

突然一點都不賢明的賢用認真的表情說了起來。賢？賢先生？對兒子使用敬語可能有些奇怪，但是面前的兒子在年齡上比我大，我也不知道該怎麼稱呼。

「這個時代的媽媽告訴了我很多事。１７歲的時候給鍬形虫一記手刀就穿越時空了。困擾的時候被警察幫助了。為了回到原來的時候而造訪自己未來的家。然後到了這裡。媽媽讓我們在這裡待命帶你去見這個時代的媽媽」
「帶去⋯⋯我難道不在這裡嗎？」
「嗯。其實⋯⋯」

賢先生的表情突然陰暗了下來。

「其實，媽媽現在在住院。醫生說她還有一個月的命⋯⋯而今天就快到一個月了」
「誒！？」

我，我正在入院而且快要死了⋯⋯怎麼會⋯⋯

「為什麼會入院！？」
「那個，媽媽讓我絕對不要說。除了對回去有必要的事情之外什麼都不能說」
「怎，怎麼能這樣⋯⋯！因為我馬上就要死了啊。要是能告訴過去的我為什麼會這樣的話，說不定就能避免這次死亡了啊！」
「我也曾說服過媽媽，不過她還是沒答應。如果能說出未來的事情肯定會有很多好處的。也不會像現在這樣臨近死亡，能避免所有的意外，還能在抽獎裡抽中大獎。不過媽媽沒有聽我說」
「為，為什麼⋯⋯」
「不知道⋯⋯連媽媽（過去）也不知道嗎」

同樣都是我，但我卻不知道我自己在想什麼。至少我認為賢先生剛才說的是對的。現在的我有很多事想重新來過。今早將窗簾拉壞了，忘了帶便當，以及激怒了悠介。如果小氦把我帶去的地方不是未來而是過去，一定能將歷史改寫。小氦你小子，這哪是舒展翅膀休息的時候啊。

「而且，媽媽讓我們在這裡等也讓我很奇怪。如果直接到媽媽穿越過來的地方等的話就能直接將媽媽帶到醫院去了。為什麼要特意讓媽媽（過去）交給警察，經由派出所來到家裡，然後再去醫院這麼麻煩呢。我們在家裡乾等也冷靜不下來啊。媽媽今天說不定就要去世了⋯⋯！」

的確。我因為來到這個時代而開始害怕，然後對戰殺戮機器，被茂盛先生侮辱我的牙齒，聞到茂盛先生的腳臭，這些全部都是不必要發生的。

「媽媽一定有她自己的想法吧」
「⋯⋯是嗎」

相對於一臉沉重的美香小姐，賢先生似乎並沒有考慮太多。雖然我也有同感，但依然很不爽。

「母親在５０年前已經體驗過這件事了，大概是知道這之後會發生什麼才這麼做的吧⋯⋯」
「嗯⋯⋯」

原來如此。美香小姐的頭腦並沒有像她的牙齒那樣靠譜。

「不管怎樣，我們先給您帶路吧，媽媽。我們都說不了，也不知道回去的方法。媽媽雖然說過不用著急，但現在的情況不允許我們悠閑。趕緊走吧！」
「好，好的！」

飯飽後的小氦爬上了我的肩上。

「這個，是這邊嗎？」

我將手伸向了附近的門。

「不，不對媽媽！那裡不是門口！那個房間絕對不能看！」

不能看的房間？裡面有不能被父母知道的東西嗎？

還有一個很大的疑問。但我沒有找到提問的時機就被三人帶到了外面。聽說醫院離這裡有兩個站。過了５０年，電車似乎還是電車。話說我能這麼走在街上嗎。茂盛先生都那麼極力去隱瞞。

「那個⋯⋯剛才警察和我說盡可能不要去了解這個時代的事情，這沒關係嗎？」

賢先生楞了一下後笑了。

「從常識上來考慮應該是這樣吧。雖然她本人有阻止過，不過看看街道的風景也沒關係吧」

是這麼一回事嗎。那我盡可能收集一下未來的情報好了。在離開綠色公寓前往車站的路上。周圍都是相類似的建築物。公路雖然和我那時候沒什麼區別，只不過車是飛起來的。真的是５０厘米左右。駕駛席上的人並沒有握著方向盤，再說也沒有方向盤。因為完全沒有發出聲響，感覺一不留神就會被撞到。至今為止除了我們之外還沒見到其他行人。

「但是鍬形虫被別人看到會很麻煩的，請注意一下」
「好，好的」

小氦在我背後。先將它藏在借來的虫籠裡，然後再放入借來的背包裡。現在回想起來，要是和美香小姐接了衣服就好了。我現在穿的水手服，根據茂盛先生所云，被這個時代的人看到會覺得很老套。不過話說回來，我兒子一家所穿的衣服和我那個時代幾乎沒有什麼變化。款式和材料都很普通。服裝方面的品味大概並沒有改變，只是水手服過時了吧。

「不過警察竟然會幫忙。穿越時空可是世界上被禁止的事情啊。雖然到了現在不管禁止還是不禁止，反正鍬形虫已經滅絕了，也做不到穿越時空」
「是個很親切的人。雖然有些奇怪⋯⋯」
「⋯⋯媽媽，能不能不要用敬語？感覺有點不習慣，我們明明是一家人」

就算你這麼說了，但對於我來說我是第一次和你們見面，而且還比我年長。對你來說我雖然是家長啦。不准在那笑嘻嘻的。

家長。家長啊。

「我⋯⋯我的，那個，丈夫不在嗎？」

終於將那件很在意的事情問出口了。你的父親，也就是我的丈夫。我的姓似乎也變了，還有兒子和孫女。這麼說肯定有丈夫。但是在兒子的家裡並沒有看見他。會不會是先到醫院去了呢。

賢先生不再笑了。

「媽媽說，爸爸的事情是最不能說出來的。比任何事都重要」

這個也不能說啊。

「只是，對於這個問題，媽媽讓我這麼回答的。『別擔心，好好期待吧』。到了醫院也見不到父親的」

這我要怎麼理解呢。擔心⋯⋯這要是暴力男該怎麼辦啊，如果滿身負債該怎麼辦？

「很害怕會阻礙到爸爸和媽媽的相遇嘛。唯獨這個情報我也不想說」

光是讓我好好期待就已經足夠了。

走了五分鐘之後人開始多了起來。因為我穿著不搭調的衣服被路人們一直看著，感覺很不好意思。雖然我也一直看著他們。大家比想像中更接近２０１６年的感覺。因為第一個見到的是茂盛先生，所以有了奇怪的印象。髮型的品味並沒有因為穿越時空而被改寫，只是那個人對髮型的品味與人有所差異。

周圍的景色開始發生了改變。類似飲食店和公司的大廈不時地能看到。但是找不到便利店和超市，完全看不到賣日用品的店舖。說真的這街道一點都不有趣。要怎麼買東西呢。因為電腦變得很厲害能在網上做很厲害的事情，所以在家裡也能買得到嗎。說起來剛才茂盛先生也能觸摸畫面裡的東西呢。

「這裡就是車站」

賢先生所指的地方是類似小中型車庫一般構造的建築物。裡面能看得到類似樓梯的東西。樸素的牌子上寫著「三垣町站」的字樣，我知道這名字。就在我住的小鎮隔壁的隔壁，並不是很遠，記得小學的時候曾和雅司騎車到那小鎮的公園裡玩。有個很大的回旋滑梯，在過彎的時候用力過猛飛了出去摔倒胳膊肘縫了四針。

但那個鎮並不是能建設車站那種等級。更另我驚訝的是，利用這個站的人還有很多。人流大得一不小心就會和兒子一家走散。過了５０年一切就會改變那麼多嗎。我想起剛才說過的森林變成街道的那件事。在這裡發展起來之前買到一間公寓豈不是非常划算？哦？終於了得到有用的情報？這樣能買得到小旅館之類的？

「媽媽沒拿到許可不能進入車站呢」

誒？許可？坐電車還需要執照嗎？

「你們先在站台等著」

美香小姐牽著小花香的手走了。

「什麼？我要在這裡考試嗎？」
「這個⋯⋯現在坐電車需要一個叫求求樂的東西，忘記帶的人需要臨時許可⋯⋯啊，很難說清楚。總之就是用那裡的機器⋯⋯」

⋯⋯又是求求樂啊。雖然搞不懂，不過那東西真萬能。跟著賢先生走到了牆邊，那是什麼⋯⋯看到了一個發著淡綠色光芒的東西。那個⋯⋯是手印？飄在空中呢。

「把手放在這個手印上」
「好，好的」

完全不知道這機器是幹什麼用的，就連是不是機器我都不知道。我小心翼翼地把手放上去之後，淡綠色變成了深綠色，發出PON的一聲。接著手印上面出現了橙色的四角形。

「我來替你⋯⋯咦？求求樂不見了」

賢先生翻弄著口袋。那是很重要的東西吧？為什麼沒有帶？每次我把票丟了的時候媽媽都會說同樣的話⋯⋯

「奇怪了，就算是我也不會忘記的啊⋯⋯是這個口袋嗎⋯⋯不對，沒有。抱歉，媽媽，稍等一下」

把手放開之後四角形消失了。看到這樣的賢先生，我對於他繼承了我的基因這件事有了實感。要是能順便把我的丈夫的特徵也表現出來就好了。可能出現了但我沒有發覺吧⋯⋯

「啊，找到了！找到了！」

賢先生從最初開始找的那個口袋裡找到了求求樂。心裡想著「鬧哪樣」的我再次把手放到了手印上，之前的四角形又浮了起來。賢先生用他那個刻著「KEN」字樣的求求樂觸碰了一下後伴隨電子音消失了。

「好的，這樣就能進站了」
「哈，哈⋯⋯」

結果還是沒搞懂我們在幹什麼，總之是獲得了那個叫許可什麼的玩意。嗯，這車票是保存在手掌上的嗎？檢票的時候要碰一下手掌那樣？

跟著賢先生走過去之後，到了剛才所看到的那個類似樓梯一樣的東西那。不過這不是樓梯而是坡道，而且地板還會動。右邊是從上到下，左邊是從下到上類似傳送帶的東西，並沒有電梯那麼誇張，只是自然地讓地板流動而已。

「這個時間人比較多，不要走散了。媽媽很擅長走散呢」

嗚⋯⋯我給兒子的印象是這樣的嗎。但是繼承了我這一點的兒子和我一起的話，這想必是一件很危險的事吧。說不定一下子就會走散然後各自開始行動。

在會動的地板上停下的人基本沒有，我和賢先生配合著周圍的人的腳步走著。景色的流動是平時的幾倍速。走過坡道，抵達地下的大廳時頭上時常會出現指示牌，上面指示著幾號線是這邊，幾號線是那邊的。看來會經過這個站的線路不只有一條。到處都是從沒聽過也沒見過的地鐵站的名字。隔著兩個站可不是單純的事情。如果迷路的話，憑感覺去選擇電車感覺之後會通往完全相反的方向。

「媽媽，要轉線了」
「誒？誒？」

賢先生輕輕地向與至今的流動完全相反的地板上跨了過去。原來不止是坡道，連平坦的地板也會流動。我慌忙地跟著賢先生後面跳了起來，然後不愧是我，直接把頭撞上了賢先生的尾骨上。

「哦萬！」
「對，對不起！」

我的兒子突然忍不住痛哭喊出了類似裝湯容器的東西。抱歉，我的失敗病毒遍佈著全身。你身體裡也被入侵了一半，你也應該明白的吧？

「忘，忘了說了，還會有幾次像剛才那樣換地板，所以注意一下。媽媽」

未，未來的車站真是麻煩。一眼望去發現我所看到的所有地板都在往不同方向移動中。原本在這個地板上，突然就換到那個地板上，有著很多的人在上面移動。未來的我能對付這種東西嗎？感覺為了抵達目的地站台拚命努力之後，結果會出了車站。

之後又換了兩次地板，再一次進入了通往地下的坡道。周圍有很多穿著西服的大叔。大概是到了下班時間吧。說起來現在大概幾點了？外面的光亮和我來的時候完全沒有不同。根據我肚子的感覺，現在應該是晚上七點，我也不知道我是到了２０６６年幾月幾日什麼時間。

離開坡道之後到了站台。咦？不檢票也沒關係嗎？應該不是免費的吧，剛才還對那個發光的手印做了什麼呢。難道有那種通過某處就能檢票的系統嗎。

站台的地板似乎不會流動。左右都有路線，站台兩段都設有牆壁，不用擔心會掉到線路上去。那是叫站台門嗎？這設備真希望能盡早在我那個時代實用於所有車站。以我這種會經常摔倒的人來看，走在車站的站台旁邊總是伴隨著死亡。

接下來，六號線通往小原的電車即將到達。

接下來，五號線通往永坂的電車即將到達。

站內通知一直在持續著。似乎左右會同時來。看來電車的系統基本上並沒有多少改變。

「美香和花香在哪裡？應該提前先到了」

賢先生在五號線附近尋找著，而我也只能跟著他一起找。過了一段時間之後，比新幹線的車頭更加銳利的車安靜地出現了。這種龐然大物以那麼快的速度行駛著居然連空氣流動的聲音都聽不到。比起聽不到車子的聲音，聽不到空氣流動的聲音更令我驚訝。這個果然是懸浮的吧。

響起了BIBO的電子音，先是站台的門打開，然後是電車的門打開。

「怎麼辦，我們先上去吧媽媽。我再用求求通聯絡他們好了」

使用求求樂進行聯絡就叫做求求通嗎？怎麼了這個時代的人，都是傻子嗎。

「親愛的！這邊這邊！」

後面傳來了美香小姐的聲音。回過頭來看到美香小姐和小花香都在笑著。六號線也已經到站並且把門打開了。

「爸爸！搞錯電車啦！」
「啊，原來是那邊啊，抱歉抱歉！到那邊去吧！」
「嗯！」

美香小姐和小花香先上了車。

「媽媽快點！上車！」
「是！」

賢先生向著反方向的電車慢跑。這個笨兒子，母親所住的醫院那裡的電車給我好好記住啊。我追著賢先生，然後發動了今天不知道第幾次的那啥──

「啊啊啊！」
「媽媽！？」
「母親！」
「奶奶！」

已經進入車內的三人，看到前傾平地摔的我喊出聲音之後的０.５秒，電車的門關上了。

那啥⋯⋯

迷路了的說。

我知道當人真正遇到意外的時候是會變得格外冷靜的。因為我自己就經常陷入危機。所以我才能如此習慣意外的發生吧？現在的我是個非常冷靜的女人。先整理一下情況吧。

我意外穿越到２０６６年，與這個年代的茂盛警察相遇，聽取了他的意見，我決定去向未來的自己打聽回去的方法。但是，經過種種曲折後來到了未來的我所住的公寓，我卻不在那，但我的兒子兒媳還有孫女在那裡。他們為了帶我去見未來的我，準備帶我到未來的我所住院的地方去，於是到了車站。車站中簡直像是迷宮，就我一個人幾乎是不可能進行移動的，由於兒子的帶路，我們成功抵達了站台。不過在他們三人坐上電車，我也準備上去的時候，一不小心摔倒了，從而錯過了電車。用一句話來總結就是「Is ａ Showtime」

兒子一家透過已經關閉了的門上的窗戶以驚訝的表情被我送走了。我以前把醬油錯當麥茶喝下去的時候媽媽好像也是這樣的表情。沒死真是太好了。

隨後電車開走了，三人離我而去。我就連輕鬆地送他們走都做不到。左右同時有車到站的關係，下車的大人流將我吸入其中。等，等一下！只要我一直在站台上等著，他們肯定會來接我的！

我無可奈何地走到剛才的坡道上。這次是上坡。哇，地板在動耶。剛才還以為自己一個人肯定上不去的，現在我卻上來了耶。到了坡道頂之後又見到剛才那個大廳。數條線路通過，通往各站台的地板交錯不清。剛才我所在的站台是五號線和六號線。無論如何都要回去。帶著決心和覺悟的內心卻被人流所抑制使我絲毫無法動彈。要是能知道去路的話倒還沒問題，可我現在連回去的路都不知道了。腦內響起了螢火虫之光的BGM。我的大腦為什麼會選這首歌？倒閉嗎？倒閉了嗎？

從下車的人流裡無法出來比困在人群裡更麻煩。因為他們既然是從電車上下來的，就是說很有可能會將我帶到站外。跟他們在一起會被帶到外面去的。反過來想一想吧。只要離開這些人不就能大幅度減小出站的可能性了嘛！我真是天才！

嘿！不清楚會到哪裡去，不過不知道也無所謂，這麼想道的我又轉到了另一塊地板上。哼，這樣就暫時放心了。然後就要冷靜地尋找去五號線和六號線的方法。頭上也有指示，只要好好觀察，我也是有可能的。盯⋯⋯盯⋯⋯啊，我上坡了⋯⋯盯⋯⋯誒？坡？哈！？坡！？

上到盡頭所看到的景色是剛才見過的。沒錯，乃三垣町站的入口是也。會流動的地板也到此為止了。在遠處看到了那片橙色的天空。我是個冷靜的笨蛋。

⋯⋯算了。雖說是回到了起點，雖說離剛才的站台的距離被拉到了極限，不過這也說明只要從起點照著剛才的路線走就能回去了吧？這個道理懂嗎？從這個意義上來說，我的行動乃大成功，可以說是１００分滿分。再次從起點踏出第一步吧。

嗶嗶！

險惡的警報音響起來。然後在我前面的空中突然出現了畫面。那個畫面上寫著「無法確認許可」的字樣。不管再怎麼積極地去理解，那信息的意思都不會從「你這種渣渣不能入站」上離開。許可似乎在哪聽過⋯⋯好像是在進站之前。「媽媽沒拿到許可不能進入車站呢」來著，那是我摯愛的兒子所說的話。剛才應該獲得了臨時許可才對，難道⋯⋯那東西的設定是一出站就會失效的嗎？

逐漸順利地走向想像中的最糟事態，真不愧是我。回望歷史應該也不怎麼找得到像我這樣擁有強烈的失敗之力的人。啊哈哈，為什麼在我哭了？這明明是榮譽。

我再次嘗試將手放到手印上面。不過從剛才所看到的情況來分析，那個橙色的四角形如果不用求求樂進行求求通的話似乎就不行，但我沒有求求樂所以以我一人之力⋯⋯在這個世界裡只要有求求樂就是萬能的，但是沒有求求樂就什麼都做不到。我覺得這很有問題！

到站台去等是不可能的了，在檢票口等也不可能，既然如此就只能在車站入口等了。我已經不會人流所帶走了。要一直黏在這裡。快點來接我，兒子。歸根結底就是因為你差點搞錯電車才會造成這種情況的。你不該叫賢，應該叫愚。

⋯⋯不過我這身衣服還是很顯眼。路過的人都用「那孩子是自願穿上那衣服的？」這種眼神看著我。剛才四人一起行動的時候還好，現在只剩我一個人對視線非常敏感。特別是那邊的大嬸，那目光都快能讓我穿洞了。嗯？她過來了。

「啊啦，你穿著的衣服真古風呀！」

向我搭話了！

「水手服在我那個時候幾乎都快沒了。你為什麼穿著這個？」

５０，不，是６０多？差不多這個年紀的大嬸。這麼說，比我還小幾歲呢。就算你問我為什麼，我也只能回答這是校服了。不過從至今為止的反應來看，這個時代似乎並沒有人穿校服。

「這，這是⋯⋯這是服裝！」
「服裝？」
「這是⋯⋯這次文化祭的話劇⋯⋯」
「啊！這個時候還能有文化祭，這學校真是太棒了」
「誒？一般都沒有的嗎？」

啊，展開對話了。大嬸稍微有點驚訝。一臉「你在說廢話」的表情。

「還會有活動的學校才少見啊。我們那個時候還是有體育祭和修學旅行的，因為很多家長說孩子會受傷，不能讓孩子在外面住之類的奇怪話。在我那時候這種觀點可能會很奇怪，但是在現在已經很普遍了」

我那個時候被成為怪獸家長的人們就已經是──毒瘤了，沒想到過了５０年居然成長到能將修學旅行給廢了。明明孩子們都會傷心的，到底是誰這麼做的。感覺球技大會被廢除的話，悠介會和眼前的食物被奪走的狗有一樣的表情。而我嘛，就算把球技大會給廢掉也沒關係啦。

「⋯⋯作為服裝道具來說做功很精細呢。難道是手製的？」
「不，不是的！這是，那個，祖母以前穿過的」
「是這樣呀。不過還真乾淨啊。好好地保管著吧？」
「是，是的！」
「話劇要加油哦。對了，不介意的話把這個吃了吧」
「誒？」

獲得了薩摩芋頭。這是薩摩芋嗎？觸感和顏色都很想薩摩芋，但是長度有蔥那麼長。究竟是像蔥的薩摩芋還是像薩摩芋的蔥呢？難道有農民對法國麵包形狀的薩摩芋抱有憧憬嗎？

「謝，謝謝」
「那我先走啦」

給了我之後，大嬸就走向了車站。唔，這根蔥怎麼辦。不對，是芋頭。這份心意我很感謝，最感謝的還是那份心意。總之先放到背包裡去，但無法完全放進去，簡直像背後背著刀的伊賀忍者。但我不準備潛入到敵城的閣樓。

話說回來，這件水手服還顯眼得能吸引人過來向我搭話嗎。背上薩摩芋之後感覺關注我的視線更多了。繼續待在這麼多人的車站前，肯定還會繼續被人搭訕的。還可能會被人盤問呢。照茂盛先生的說法，我不能和這個時代的人說太多話。從過去穿越而來的我或者小氦被發現的話，就會可能引起世界級的大事件。快點來啊，賢先生。不對，是愚先生。

汪！

嗯？不是愚先生而是狗的聲音。是類似小狗的可愛叫聲。狗嗎⋯⋯作為隨從陪我到鬼島上去吧。女高中生加狗加鍬形虫加薩摩芋能拯救世界嗎？想著這些有的沒的於是我開始尋找聲源。感覺就在附近，但是找不到狗在哪。腳下倒是有個四只腳會自動行走的謎之機器。

我已經知道了，接下來肯定會發生些壞事。就算是俺也學乖了。

汪！

這聲音果然是從腳下的謎之機器那裡發出來的。尺寸和造型的確很像狗，不過因為是機器，並沒有長一根毛，全身還是綠色，還穿著者銀色的盔甲。

好可怕。現在它只是對著我喊了兩聲而已。儘管看不出有向我攻擊的意思。但我還是害怕。但是一不小心的話可能情況會變得更早，所以我不敢動。總之先離開它一步試試吧。也許並不是對我，而是對著我身後人類無法看得見的某些東西在叫呢。那個從其他意義上更可怕。我走⋯⋯

沙沙⋯⋯

⋯⋯跟過來了。那雙眼還在看著我，從那金屬制的表情裡看不出任何情報。這只機器狗到底是怎麼回事。就算我對它吐槽它也不會理我吧，我已經學乖了。大概是流浪機器狗之類的吧。流浪機器狗是啥？那種東西到底有什麼製作的必要。從周圍的人都對這只狗完全沒有反應看來，看來這只機器狗的存在就像天空上飄著云一樣，是那麼理所當然的事情。

我嘗試著再移動一步，機器狗還是跟了過來。向著反方向移動兩步回到原來的位置，它還是跟過來。這只狗是把我當作捕食對象了嗎，還是說想先拿我當磨牙的工具，或者是覺得拿我來試導彈感覺正好？不管怎樣我能選擇的只有Fly Away。

目光對上了，我後退了。狗配合著我的步伐前進了。再退一步它就再進一步。飛快地退後就飛快地前進。

「什，什麼？」

問下動機吧。雖然是對剛才的清掃機器不管用的招數，但這次是生物外形的機器。多少理解一下人的語言也沒關係的。傳達吧！人之心！

「煩死了」

⋯⋯嗯？

「煩死了，女人」
「啊⋯⋯」

語言相通了，可這下麻煩了。

發出GIGIGIGIGI的聲響，機器狗抬起了上半身用兩只腳站立了起來。身高大概到我腰部，抬頭盯著我。我做了什麼錯事？那個⋯⋯

機器狗走向了我的背後，差點被慢了一拍的機器狗抓住我的背後把肉給咬掉。我背後現在還有小氦在呢，饒了我吧。

「快讓我吃！！」
「誒！？這條狗怎麼回事！？」

瞄到那使用雙腳奔跑的姿勢感覺非常地優美，就像田徑選手一樣。雖然不知道是誰的作品，可你要做的話至少也做成像狗那樣跑步啊！腳太短了跑得好慢！

不過那邊慢對於我來說是好事。單單是直線就能甩掉。只要是直線。很抱歉現在是下班高峰期的車站。不用手把人撥開的話連前進都做不到。這一點對那機器狗很有利。那傢伙更加地敏捷，小小的身形也能巧妙地穿過人與人之間的間隙。如此計算的話，這場比拼大家不分上下。如果能盡早離開人群就是我贏了。附加獎勵「迷路體驗（強制）」。如果被人群攔住了就是我輸。附加獎勵是死。給我設下這種毫不親切的規則的地方就是２０６６年。

大概跑了１００米。人少了起來，跑起來也輕鬆多了。不可思議的是路過的人一點都不對這個狀況感到擔心。對於他們來說「機器狗不由分說地追過來是常事」大概很正常吧。夠了，那樣的時代到來之前我大概就翹辮子了。

抵達十字路口之後我轉向了左邊。因為看上去威脅最小的地方就是左邊。與狗之間的距離已經有１５米遠了。我將背上的背包抱在了胸前。

「小氦，抱歉！很搖吧？」

為了不給背包中虫籠裡的小氦造成負擔，為了能全力奔跑，我將背包緊緊地抱住減緩衝擊。我的運動神經絕對不好，應該說是渣渣中的渣渣，但我也不是白遲到那麼多年的。緊急時刻全力奔跑這種事我已經習慣了。

「你給我站住啊！！！」

在後面奔跑著的機器狗發出了成年男性的聲音。不要忘了你的登場劇情啊！我們可是在用小狗那種可愛的叫聲在進行交談啊！角色已經崩壞了混蛋！

「擎天柱形態！超加速模式！」

聽到後面發出了很挫的叫聲之後又聽到了金屬摩擦碰撞的聲音。好奇地回過頭後，發現那傢伙變形了。剛才還是用著狗的體形在勉強地用雙腿跑著，這回是狗的跑法，不，那已經是狼了⋯⋯比之前大了不是一兩圈，而且很快。越來越搞不懂這機器是為了什麼才被製造出來的。至少能肯定不是拿來愛玩用的。請不要隨便將這種搞不懂是兵器還是什麼的東西扔在車站前。

找到拐角就馬上進行拐彎，跑入小道裡，盡可能地去甩掉它，但是１５米的差距漸漸就被縮小了，每次回頭都看到鐵製的狼在接近我。這樣子周圍的人多少都能注意一下了吧。這麼想的時候，周圍完全沒有人了。看來離車站已經很遠了。我一個人能回得到車站嗎。不，現在不是說那個的時候。我已經累得差不多了。

「不要藏了！！！」

從後方傳來的成年男性一般粗狂的喊聲。那個聲音已經非常地接近⋯⋯嗯？藏？

「快把那個薩摩芋交出來！抱得那麼緊幹什麼！」

原 來 是 這 樣 啊！

一開始的目標就是那根像蔥的薩摩芋啊。我抱著的不是這根像薩摩芋一樣的蔥，而是我最愛的小氦啊笨蛋！雖然對那個機器的身體能否將芋頭轉化為能量抱有疑問，但那只機器狗的眼神，是自認為自己的身體構造能夠消化吸收薩摩芋的眼神。

「這，這個！？」

我握住露在外面的薩摩芋把它拔出來向後面扔了出去。然後機器狗發出了可愛的小狗才會發出的聲音，用跳接將薩摩芋接住了。一邊吃著一邊變回原來的狗樣。這就⋯⋯得救了？我站在離機器狗稍遠的地方看著它。既然能說話那一開始就跟我說想要不就好了。從開始到最後都是讓人難以理解的傢伙。我決定一輩子都不原諒這傢伙和把薩摩芋給我的那個大嬸。

完全迷路了。跑的途中不知道衝進了哪條胡同裡。為了甩開那條狗，不知道拐了多少個彎，現在完全不知道回去的路。如此奔放地離開車站，縱使那笨兒子有再高的智商也找不到我吧。也不能借助警察的力量。要是有求求樂的話就能使用求求通，不知道有沒有公共求求樂用呢？就算有，我也沒有錢，也不知道怎麼用，還可能會被再次襲擊⋯⋯對著時代的機器沒有一點好印象。

向人問路也不怎麼好。只要這個背包裡藏著鍬形虫，整個世界都可能是我的敵人。盡可能不要和人還有機器扯上關係，靠自己找到車站吧。不過周圍也沒有人能被我扯上關係的。路邊要是有地圖之類的就好了。

被高聳的大廈所夾住的道路。路小得連車都很難通過，步行道和車行道並沒有被區分開。天空雖然明亮，但這條路上有著影子，那氣氛簡直就不適合妙齡少女在這裡行動。既然有著那樣凶殘的犬型兵器在街上，流氓估計都絕種了吧，但是有著那麼多凶殘的犬型兵器在街上，比流氓還要恐怖百倍。尋找著大道的我遇見了丁字路口，我憑著感覺選擇了右邊的路。然後在那邊見到了人影。

二十出頭體形纖弱的男性。顏色鮮艷的中度髮型隨意地散開，感覺沒有怎麼整理過。衣服也不怎麼體面，有種去附近的便利店時穿的便裝的感覺。表情似乎有點疲憊，說成憔悴可能更貼切。很想搞砸了什麼事的我那樣，在路邊抱著雙腳坐著，那個神情讓我覺得他並不陌生，不過今天還是覺得陌生比較好。看來的確有什麼。

有，什麼。這並不是抽象意義而是現實意義上的。可能也是這個人如此失落的原因。這個人的額頭長著一些不尋常的東西。極其漂亮的，有一米高的，枇杷枝。的確有４，５個漂亮的分枝。完全不知道為什麼會長出那樣的東西，於是我放棄了思考。在這個時代肯定是正常的事情。

⋯⋯嗯，感覺也不方便問路。如果現在和這個人說了話，１０秒之後可能就會發生追逐戰。想要在這個時代活下去，就必須盡可能不去接觸那些奇怪的東西。已經上過好幾次課了。無論我再怎麼天真地去思考，這個人會和我意氣相投這種事的概率就像天文數字當分母那麼低，總之還是小心點為好。看到這個人的樣子，感覺會突然出現一些非常不好的事情，所以我盡可能自然地，不要和他對上目光，慢慢地走過去。

我裝作很平靜地走著。一點，一點地接近。他並沒有看我。這樣下去能行。還剩１米，我終於過去了。太好了！內心滿心歡喜，但表情非常淡定。接下來盡可能快地離這個人遠點。

「喂！」

嗚哇！他對我說話了！

「那邊的女生！稍等一下！」

我戰戰兢兢地轉了過去。

「有⋯⋯有什麼事嗎？」
「你看到人的頭上長枇杷就不會覺得奇怪嗎？」
「誒，啊⋯⋯誒？」
「對於別人的頭上長了枇杷這麼奇怪的事情你就什麼感覺都沒有嗎！？」

那當然有啦！雖然無法說出口，但這些疑問都是有的！這是怎麼回事？這個人陷入這種狀態在２０６６年也是極其異常的事情嗎？還是搞不懂標準是什麼。被變形的機器狗追是習以為常的事情，而額頭上長枇杷枝就是緊急事件？

他站起向我走來。要跑嗎？不，我已經不想再玩追逐戰了。對手還是人類，很難甩開的。而且交給警察處理的話麻煩的人是我。

「⋯⋯今早起來的時候就長出來了。枇杷這東西自從以前在食堂出現過一次以後就再也沒有吃過了」
「這個⋯⋯」

開始說明了。

「你知道額頭上長枇杷的時候該怎麼對應嗎！？我不敢拔掉！感覺根部已經長到我的腦裡了！」
「不，不知道！叫救護車過來如何！？」
「醫院！？這種人類史上沒出現過的病例，根本不知道他們會怎麼處理我啊！要是因為醫生的好奇心被抓去做人體實驗的話你要怎麼賠我！」

不，我賠不了你。感覺已經有點自暴自棄了。看來他真的很苦惱。可是這個時代都不可能發生的事，我怎麼可能有辦法。

「就算是這樣，還是早點去給醫生看看比較好啊。放著不管肯定很會危險⋯⋯我又不知道其他的方法。再見」

我轉過身去再次準備離開。交流過之後，知道他不是那種憑著自己的意願長出這種奇怪東西的怪人。應該是被這片危險的天空下所誕生的新種植物入侵了，真是可怜的人。但我還是不想繼續和他扯上關係。我身上的意外已經多得不行了。

｛等，等一下！不要說這種話啊，和我一起想想辦法吧！？要怎麼做才能離開這些枇杷啊！｝

嗚！我想離開你啊！我知道你很苦惱，可我現在也非常苦惱啊！必須快點到車站，快點到醫院去啊！

「現在我所知道的情報就是，除草劑首先不會起作用」

又，又開始說明了⋯⋯

「我見完全沒有效果，就用水清洗掉了，結果它們靠這些水成長了」

⋯⋯那還真是麻煩呢。

「還有就是！似乎它們在和我的精神狀態進行著密切的接觸，我情緒低落的時候就會飛快地成長！」

這枇杷性格真差。

「今天一天因為這些枇杷的關係我失落了一整天。你看這果實！已經不是普通枇杷的大小了」
「⋯⋯反過來說高興或者歡笑的時候就會枯萎吧」
「什！」

一心想要離開那個地方我的，說出了近年來最隨意的一句話。這麼想的時候⋯⋯

那位小哥一副類似「就是這個！」的表情。

「原來如此！可能真是這樣！你是天才啊！」

那還真是謝謝了。該放我走了吧？

「我趕緊試試」

說完他便將雙手伸向天空，然後⋯⋯

「哇哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈啊哈！」

開始大笑了起來。走投無路的人原來會這樣笑啊。

滋啪啪啪啪！

傳來了衣服被撐破的聲音。額頭上的枇杷枝還是老樣子。發生變化的是他右邊的胳肢窩。那裡又生出了一根並撐破了Ｔ恤。

⋯⋯這個世界上神佛皆無。

「笑了之後增加了哦？」
「啊，這個⋯⋯」
「笑了之後增加了哦？」
「嗯，嗯⋯⋯」
「你怎麼賠我啊！」
「誒誒！？」

怪我咯？太沒道理了，我開始想反駁他了，但是⋯⋯他的眼裡充滿了「不想死，不想死」的想法。他保持著舉手的姿勢，因為右胳肢窩長了枇杷的關係，無法把胳膊放下來，保持著那個姿勢，臉上混雜著眼淚的鼻涕，為了支撐住右邊偏重的身體，左邊正在咬牙切齒。小事倒是無所謂，變成了這樣連我都開始想幫他了。

「嗚嗚⋯⋯都怪你！都怪你讓我笑！」
「對，對不起！沒想到事情會變成這樣⋯⋯」

與他失落的心情相呼應，額頭和右胳肢窩的枇杷都漸漸地成長了。這枇杷的性格實在是太差了。

「叫救護車來吧！已經別無他法了！」
「好，好吧⋯⋯你能幫我叫嗎？我右臂動不了」
「那個⋯⋯這個時代能用手機嗎⋯⋯」
「哈？手機？這麼緊急的時候你在開什麼玩笑呢！？」
「沒沒沒沒有這個意思啦！」
「快點用求求通啊！」
「我沒有啊！」
「在這個時代哪還有人會不帶求求樂啊！別開玩笑了！」
「我，我，那個⋯⋯」

一定要說明原因嗎？可以的話，不怎麼想說啊⋯⋯

「那位男生！不要那麼大聲啊，那位女生很苦惱呢」

背後傳來了其他人的聲音。我和枇杷男對視了一下之後安靜了下來，同時看向那聲音的主人。

年輕的男人。和枇杷男的年齡不會相差太遠。被長長的瀏海遮住了一點的眼睛很大，很標緻的輪廓，非常勻稱的臉龐。身材也很高很苗條，上下一套的運動衫只是普通的運動衫，但被他穿起來卻感覺很時髦。一身充滿自信的打扮，是自從我到了這個世界之後感覺最可靠的一個人。

「我聽到你們說的了。剛才我也叫了救護車。似乎很麻煩啊，沒事吧？」
「誒，啊，啊啊⋯⋯仔細想想只是枇杷重了點，沒有其他問題」

不過我想那已經是很大的問題了。

「我知道你很著急，但也不要對毫不相干的女生怒吼啊。你也沒事吧？」
「我，我沒，什麼」

他說話很乾脆，感覺很帥氣。不小心看入迷了。

砰。

有什麼東西落入了我的視線。

「啊」
「啊」
「啊」

異口同聲。只能如此形容。枇杷男額頭和右胳肢窩的枇杷枝都掉下來了。就像頭髮落下一般。沒有出血，好像也沒有疼痛。突然健康了。

「剛剛才叫了救護車的⋯⋯」

就連凋零時這枇杷的性格還是那麼差。

很快救護車就從空中趕到帶走了枇杷男。雖然一眼就看出沒有再長了，但還算不上是不需要急救的人。救護人員們當然不相信那些枇杷是從人體上生出來的。結果被懷疑了一番之後，我和那位小帥哥都成為了證人，說是要進行一番精密檢查，於是前往了醫院。

我和小帥哥被留下了。

「⋯⋯那個人到底怎麼了？你知道那些枇杷是怎麼生出來的嗎？」
「不⋯⋯完全不懂」

看來他也不懂。到了這個時代以後，總是遇上一些意義不明的意外，而且到最後還是沒搞清楚原因，一直重複著這些事情，讓我有點消化不良。不過我無論如何也不想知道。

「話說回來，你穿的衣服還真是少見呢。是叫水手服嗎？」
「是的，你居然知道」
「之前在書上看到的。好像之前是水手穿的服裝，不知道什麼時候卻變成女學生穿的校服了。記得是我奶奶那個時代才有學校規定穿著這些去上學的」

是的，的確有。你真是清楚。不是對Cosplay狂熱者就是非常博學了。希望會是後者。

「那個，謝謝你剛才幫了我」
「哈哈。沒事沒事，我看不下去嘛」

這可能是我來到這個時代遇到的最靠譜的人。長得帥腦子聰明，身材也棒，學校裡要是有這樣的前輩我肯定會很向往的。

「再見，我要到車站去。時間已經不早了，你小心點吧」
「啊，我也準備去車站的，可是迷路了⋯⋯」
「是這樣啊！那我給你帶路。跟我來」
「可以嗎！？非常謝謝你！」

哇！真是溫柔啊！這次穿越時空終於有了些好的回憶呢。回去之後就和這個人的先祖結婚好了。

「你叫什麼名字？」
「淡路奈津美」
「我叫內山。工作是在小胡同裡找到少年少女將奇怪的蘑菇以勉強合法的價格賣給他們」

⋯⋯嗯？

「你現在身上有多少？」

開始啦。第二回追逐戰的時間到了。請不要以玩一玩的心態參加比賽，要有賭上生死的覺悟才能參加哦。我靠！差點陷入危機了！這傢伙是至今為止最為危險的人物啊！

「等一下啊！第一次我會算便宜點的！這蘑菇很不錯的！」

用詞還是那樣子，說話方式依然很清爽，乾脆，但內容卻黑得可以。

「幾乎沒有毒性，有毒物質也幾乎沒有啊！」

勉強你大爺啊。現在只是法治整頓沒及時，你那東西是直接被禁止的吧！過了５０年法律還是毫無進展啊！

轉角之後再轉角，為了盡可能地擺脫他。必須想辦法離開他的視線。面對一名成年男性，我可是瘦弱的女高中生啊。而且剛才又跑了一大堆的路。現在已是方向感全無。雖然選擇方便逃跑的路線是好事，但勝負拖得越久就越不利。

「至少聽我說一下啊！你看看啊！這就是那些蘑菇！」

逼近丁字路口的時候，我的右耳突然穿過了一顆紫色的蘑菇。撞上大廈掉落地面的那顆蘑菇，散發著無論多餓都不希望去攝取的氣場。

「噠！！！！」

喊出與可愛無不相關的聲音，我反射性地轉向了左邊，那裡居然是死胡同。我慌忙轉向反方向的時候，爽朗地散發著汗水的內山已經將道路堵住了。

「終於追上你了」

嘻嘻地笑著。

「不好意思嚇到你了。但我覺得你是誤會了。這個蘑菇對身體很有幫助的。只是吃下去就會有很不可思議的感覺，全身的肌肉都會興奮起來，使身體能力暫時提升」
「那，那東西對人體無害嗎？」
「那當然！今天我破例免費送給你。之後你一定會想要的，那個時候在這附近游蕩的話記得找我哦」
「這種東西我不要！」

內山在逐步地逼近著。我漸漸地後退著，最後被逼到牆邊。

「嗯，你就不能察覺這蘑菇的魅力嗎？最好就是吃下去，但是生的又不怎麼好吃⋯⋯其實聞聞味道也能其到一點效果哦，要試試嗎？」
「我拒絕！」
「不要說這種話啊，來」

背後貼在牆壁上的我被他壓著，拿著蘑菇接近著我的鼻子。

「住，住手！」
「擎天柱形態！超強模式！」

從內山的背後傳來了剛才聽到過的很挫的叫聲。

「追尋著食物的味道過來一看，這不是剛才那個女人嘛。現在是什麼情況？」

滿身綠色加銀色裝甲。成年男性吐痰時渾濁的聲音。恐怖機器狗是也。不過現在的身體並不是狗的形態。而是變成了滿身肌肉的大力士一般，用兩條腿走路的生命體（？）

「好不容易趕過來的，可那種蘑菇可吃不得啊。那邊的小哥也散發著不能吃的味道」
「找我有什麼事。不要壞我的好事」

內山放開了我轉向了機器狗。

「你在那裡幹什麼，那個女人是我的食物來源」

能不能不要接連表示我就像是給你提供食物一樣的人呢？不過這也許是個機會。

「救救我！這個人會賣一些危險的蘑菇！」
「看在剛才那些芋頭的份上就幫幫你吧。快點走」
「嗯，嗯！」
「站住！」

內山抓住我的手腕的那只手被機器狗咬住了。我一心只顧著逃走，決不回頭。從背後傳來的機器狗的行動聲也漸漸地聽不到了。

感覺跑了很遠。同時一想到有可能裡車站更遠之後，內心只有郁悶。我到底在幹什麼。現在不是幫助可疑人物或者被他們幫助的時候啊。我還得盡早去見未來的那個今天說不定就會死的自己啊。

在十字路口拐了個彎之後看到了河。這條河有點熟悉。這難道是剛來到這個時代的時候見到的那個？穿過矮樹叢進入散步道。果然是同一條河，同一條道。

內山大概不會再追過來了，不過以防萬一還是再跑一段路吧，跑了一段路之後就看到了橋，我藏在下面坐著。我累了。發生了太多可怕的事情。

我將小氦從背包裡放了出來。愛虫飛到我的手掌上張開了翅膀。小氦也累了吧。一直待在狹窄的籠子裡，而且還搖搖晃晃的，

「接下來該怎麼辦呢⋯⋯？」

就算問小氦也沒意義⋯⋯我已經不知道該怎麼辦才好了。車站的方向已經完全不清楚了，估計距離也非常地遠吧。一想到還有可能遇上可疑分子我就害怕得動不了。

「我或許回不去了⋯⋯」

向爬上左肩的小氦投去放棄的眼光的時候。

（夾）

「好痛！」

被小氦夾住了鼻子。是屬於愛撫的那類。

「⋯⋯不能放棄，嗎？」

感覺小氦在說「嗯，不行的」。但是⋯⋯

「不可能的。已經沒有可以依靠的人了⋯⋯」

這麼說的時候，小氦似乎也有點失落。好想喂小氦吃平時吃的那些啊。我也想喝點什麼，更希望能洗個澡。然後像今天這樣普通地去上學。雖然一直重複著錯漏百出的每一天並後悔著。但我還是想回去。

「⋯⋯把我帶到這裡來的人是你吧，小氦」

小氦沒有回答。

「為什麼要讓我看這個時代？」

小氦沒有動。

「我希望重來，但為什麼要把我帶到未來？」

小氦沒有出聲。

有很多事想問的。既然過了５０年，要是有可以與昆虫對話的機器就好了。我完全不知道小氦在想什麼。

「能把我送回原來的時代嗎？」

溫柔地敲了一下手刀卻什麼都沒有發生。小氦一副什麼都不知道的樣子在夾著水手服玩。

「真是的！」

破罐破摔地喊著回去！回去！的同時一直重複著手刀，卻還是什麼都沒發生。平時都是那麼充滿率直的小氦，偏偏這次一點都不聽我的話。為什麼？就那麼像讓我看那東西嗎？在這個時代裡？

「你在那裡幹什麼！」

誒？

某人的聲音和我在橋下的叫聲混雜了起來。嚇了我一跳，回頭向那裡望去。是曾聽到過的聲音，以及見過的輪廓。

「茂盛先生！？」
「淡，淡路君！？」

英雄登場了。