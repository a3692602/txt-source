從拉爾克邸的隱藏階梯下到地下二層，這裡是一個僅放置了桌子和椅子，牆上掛著繪畫的小房間。
地板和桌面上布滿灰塵，似乎已經有十年沒有人來過了。


 「非常抱歉佩特羅大人……我最近一次進來這裡，已經是自父輩還活著過來大掃除的時候了……這個房間已經被完全棄置……。形式上是規定除了客人，領主以外的人不能進入這裡……我現在馬上就請理一下」


拉爾克不停地向佩特羅低頭道歉。


 「無妨呢。現在不是在意這種事情的時候……請你趕快出去吧，拉爾克男爵」


 「是、是的……」


拉爾克低下頭，走出小房間。
按照他所說，這裡似乎是琺基男爵家的秘密會談室，是琺基男爵家的一代目當主，拉爾克的祖父在晚年時建造的。
房間牆壁上刻畫了魔術式，賦予了對抗監聽魔術的特性。
包含之前的美術倉庫，拉爾克的祖父先生還真是個可疑的人物。


 「佩爾特魯卿……請坐下來吧」


我抬起放在桌上的手，招呼佩特羅入座。


 「你這樣讓我感覺有點發抖所以就別這麼稱呼我了……怎麼說，你很喜歡搞得這麼煞有其事呢」


佩特羅從面具後窺視過來，嘴巴像是很厭煩似的扭曲。


 「好厲害！怎麼說，有點讓人很興奮呢！梅婭在小說裡讀到過這樣的房間！」


眼睛發光的梅婭在我旁邊喧鬧著。
我們之前相互說好了可以各自帶一個人過來。比如，現在佩特羅旁邊就站著繆西。


說實話，繆西小姐是個很普通的人，要肩負起佩特羅所旨的酋雷姆伯爵討伐計劃是不是會有點負擔太重，我不禁這麼認為。
至少得要帶上雅爾塔米亞程度的秘書才足夠吧。
嘛，我也不用想太多。
要是深入說這個話題，結果肯定會說到佩特羅自身也是力有不逮。


 「沒問題吧？這個孩子在這說個不停呢？你啊，最近在國內的動作太誇張了，迪恩拉特王家那邊似乎挺不高興呢。如果做的太過分的話，我的立場會很危險的說……」


我還以為他承受了王家相當大的壓力，沒想到居然被厭惡到這個地步了麼。
不過也是當然。看來佩特羅自己也是，肆意地擺弄各種形跡可疑的事情，強行向現在的迪恩拉特王索取許可吧。

原智慧與破滅的惡魔，現在被我用結界關在倉庫的禁魔導書的解析用奴隷的佐羅摩尼亞，她曾經悄悄告訴過我『佩特羅那傢伙，擅自把犯罪者從牢裡放出去當他的部下，指使其去搶奪妾身的魔杖之後，再不講理地據為己有呢』。
相比於這個人的所作所為，我稍微要求取得魔術的行使權限的行為顯得可愛多了。


 「梅婭，如果不是被佩特羅先生背叛了的話是一直信賴佩特羅先生的噢」


 「你……還真是很自然地變成毒舌了呢」


佩特羅先生像要蒙混過去一樣咳了咳。
抱著深厚善意給予協力，結果卻連梅婭一起要被殺掉，這件事我可沒有忘記呢。
那個時候，佩特羅若不是最後說「放過梅婭也可以」，我也不會像現在這樣與他保持協作的關係。


 「不過……你的立場真的這麼危險嗎？」


佩特羅是我的贊助人。要是他自己陷落了，失去了利用價值我會很困擾。
正因為他有權力，不然我才不會配合這個說謊的腹黑娘娘腔的野心。


 「……表面上他們沒什麼意見……不過王家好像隱約在謀劃把我逐出王政呢。下任迪恩拉特王的第一候補，夏洛特王女是個強敵……因為她至今為止未有顯眼的動作，頭腦也不太好，周圍對她也沒有太大的期待，所以我也沒有管她……但她居然是匹黑馬呢。正義感奇怪的強，似乎十分敵視我和王國的暗部呢」


佩特羅一邊說著，一邊捂著額頭。


 「佩特羅大人……請不要在亞貝爾面前如此示弱……！」


繆西慌忙地把手伸到佩特羅面前。


 「也對、呢……支配庫多爾神失敗和被伯爵襲擊對我打擊很大，稍微變得軟弱了呢……」


佩特羅深深地嘆息。
嗯哼，夏洛特王女麼……聽到了挺有趣的事情。
複雜的政治我不太懂，也不覺得自己會與王族扯上關係，不過稍微記住也不會有什麼損失……啊咧，總覺得在哪裡聽說過這個名字。


 「亞貝爾……夏洛特王女不就是，之前那個，加斯通王女嗎？」


梅婭小聲地在我耳邊說道。


 「啊」


第四子夏洛特王女……就是那個聽說了加斯通的傳言而把他錄用為側近騎士，為了炫耀而以自身的權力強行通過，擅自把他任命為傳說級冒險者，結果在王都的大會裡敗露出糗而惱羞成怒的大笨蛋王女。

那、那個人，明明搞了一出鬧劇，居然還能成為第一候補？
額，難不成是因為我？我把另外的王子的側近騎士布萊恩打倒了並把他逼得引退，兜兜轉轉結果是幫了加斯通，我當時聽說到那件事還很高興，但難不成之後加斯通就強行當上那個王女的表面側近騎士嗎？
這、這難不成，都是我的原因？


看到我的表情，佩特羅不解地偏頭。


 「怎麼了亞貝爾醬？」


 「沒、沒有！什麼事都沒有噢！什麼都！我什麼都沒做啊！」


 「是、是嗎，那就好……不說這些閑話了，我們可不可以進入主題，亞貝爾醬」


佩特羅坐到座位上。繆西也對佩特羅和我禮貌地致意一下，也坐了下來。
我身體前傾，握住雙手。


 「佩特羅先生……現在是不是該，稍微定下諾言呢？我也是很希望能與佩特羅先生好好相處的喲？」


 「嗯……我知道了。對你說謊的話，之後會發生慘劇的呢。就在這裡，正式地、清楚明白地說好吧。你在成功討伐利維伊和酋雷姆伯爵之後……我會將奧爾庫諾亞大監獄裡監禁的重要犯罪者……本拉多·波裘解放出來，無論使用什麼手段。雖然嚴格地說，他現在是被護送的狀態」


佩特羅語氣為難，但又堅定地說道。


 「我了解了」


我輕輕點頭。


 「亞貝爾！？」
 「佩特羅大人！？」


坐在我旁邊的梅婭，以及坐在佩特羅旁邊的繆西一起露出驚愕的表情站起來。


 「快、快醒醒吧亞貝爾！為什麼、為什麼呀！梅婭可沒有聽說過這種事情啊！明明、明明說了只要有梅婭就夠了什麼的！事到如今還不行嗎！那種矮小的老頭有什麼好的！」


梅婭抓住坐著的我的肩膀，不停搖晃。


 「但但、但是！佩特羅說過，他為了佐羅摩尼亞而將犯罪者從牢裡物色出來當部下，我就想嘗試一下看看行不行，所以就不小心……啊啊！停下來！再搖下去肩膀會脫臼的啦！」


梅婭心情崩潰地不停搖晃我的肩膀。
椅子發出咔嚓咔嚓的聲音，十分可怕。
這，倒下去的話會撞到頭的。這裡都是石頭地板，可不是開玩笑的。


 「佩特羅大人！您是認真的嗎！您不是說了已經摸清楚狀況，亞貝爾什麼的隨便就能玩弄在掌心的豪言壯語了嗎！這不是一味地在讓步嗎！本拉多那傢伙，搞不好的話可是會成為超越教皇薩特利亞，在思想和魔術上的最惡劣的超級危險人物啊！？您自己也這樣說過的吧！？明明好不容易才奇跡般地成功捕獲，為什麼要特地做出像是把偶然發現的金塊扔進海裡一樣的暴行啊！？為什麼被那個狂人那樣地宣言之後，一點都不和我商量啊！？」


 「沒、沒辦法不是嗎！就算說了也不會改變，就像現在一樣！繆西！我，討厭不聽話的孩子，不要對我的決定多管閑事！不管怎樣，話都已經說了，不可能撤回！這傢伙是肯定不會聽的，他就是這樣的傢伙！極端地說，只要利維伊和酋雷姆伯爵的事情能夠解決，其他的都是小事！我只能這樣做了！我明明為這個世界如此努力了，庫多爾神卻冷面相待，某個國家又不聽勸、聽從伯爵的誘惑準備與迪恩拉特王國掀開戰爭，現在笨蛋王女又排斥我，亞貝爾醬又是一副事不關己的樣子！那由繆西來打倒它們吧！把酋雷姆伯爵幹掉吧！對，那樣的話我就馬上把話收回去！喂趕快去啊！快點啦！」


 「請不要說出小孩子一樣的話來啊！您能夠對解放了本拉多後對世界造成的影響負責任嗎！？」


 「說得好像是我抓住的一樣！第一，這種事情相比於放生亞貝爾醬簡直就是誤差一樣的東西！一億或者一億加十，沒人會覺得有什麼不一樣吧？稍、稍微把手放開！放開我！」


我看向旁邊，佩特羅也被繆西揪住了脖子。


——於是，魔術師的我，與原教皇佩爾特魯卿經過機密的會談，於琺基男爵家的地下二樓，達成了黑暗的交易。
