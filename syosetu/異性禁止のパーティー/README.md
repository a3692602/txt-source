# novel

- title: ミルク多めのブラックコーヒー
- title_jp2: 異性禁止のパーティーを作ってみたけど、ウチのメンバーどこかおかしい。 ミルク多めのブラックコーヒー
- title_zh: 組建了異性禁止的隊伍，但我們的成員有些不對勁
- author: 丘野　境界
- illust: 東西
- source: http://ncode.syosetu.com/n8304fa/
- cover: https://images-na.ssl-images-amazon.com/images/I/71aGFjOjemL.jpg
- publisher: syosetu
- date: 2019-08-23T11:58:00+08:00
- status: 完結済
- novel_status: 0x0101

## illusts


## publishers

- syosetu

## series

- name: ミルク多めのブラックコーヒー

## preface


```
「可以做到嗎!」
比起打倒敵人，更傾向於援助己方的祭司席爾瓦·洛格爾被別人指出「因為不能打倒敵人」，而被提議減少席爾瓦的報酬。
自從新加入的商人少女諾瓦來了之後，隊伍完全變了個樣。
席爾瓦決定離開變得愚蠢的隊伍，與自己的朋友狐獸人桔梗·夏目和中途參加的鬼族菲蘿，和身穿靈活鎧甲的泰蘭一起組成了新的團隊。
通過對之前隊伍的反思，席爾瓦規定了禁止異性加入隊伍這條規矩，但實際上……。

「やってられるか！」
敵を倒すよりも味方を支援することに特化している司祭シルバ・ロックールは、まさにその「敵を倒さない」ことを指摘され、シルバの報酬を減らすべきだと提案された。
新しく参加してきた商人の少女ノワが入ってから、パーティーはすっかり変わってしまった。
馬鹿馬鹿しくなり自らパーティーを離脱したシルバは、友人である狐獣人のキキョウ・ナツメや飛び入り参加してきた鬼族のヒイロ、動く鎧のタイランらと新たなパーティーを結成する。
前のパーティーでの反省から、異性禁止のパーティーを、と考えたシルバだったが、実は……。

本作はArcadiaにかおらて名義で連載していたモノに、加筆と修正を行った内容となっています。
```

## tags

- node-novel
- syosetu
- コメディ
- ハイファンタジー
- ハイファンタジー〔ファンタジー〕
- ハーレム
- ファンタジー
- 冒険
- 剣と魔法
- 完結済
- 残酷な描写あり
- 男装
- 異世界
- 異種族

# contribute

- 簡拉基茨德
- 白毛
- 

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1

## syosetu

- txtdownload_id:
- series_id:
- novel_id: n8304fa

## textlayout

- allow_lf2: true

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n8304fa&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n8304fa/)
- [我们的成员有些不对劲吧](https://tieba.baidu.com/f?kw=%E6%88%91%E4%BB%AC%E7%9A%84%E6%88%90%E5%91%98%E6%9C%89%E4%BA%9B%E4%B8%8D%E5%AF%B9%E5%8A%B2&ie=utf-8&tp=0 "我们的成员有些不对劲")
- 

