“暫且不說芙拉朵，伍德，你沒必要對我道謝。我們就是這樣約定好的吧。”
我盡量以平穩的語氣說著。

雖然知道是自欺欺人，但是如果以滿臉沉痛的人為交談對象的話，還是會更添一分顧慮的吧。如果是性情溫和的伍德，那就更別提了。
「……」
塞雷爾默默地拉著哥哥衣服的下擺。那個表情，顯得有些痛苦。

沒錯，我和芙拉朵為什麼會和這對兄妹一起生活在貧民窟裡呢？這並不是因為他們是紋章教徒，也不是因為他們是拉爾格·安的熟人。更不會是因為他們都愛慕著我。

我和他們是彼此間簽訂契約而建立起的關係。
我給予他們從拉爾格·安處得到的支援物資，以及對他們兄妹進行保護作為條件交換，而他們則對貧民窟進行介紹並作為我們與貧民窟居民間的緩衝，就是這樣的契約內容。

以貧民窟的居民為對象，交換這樣的長期的契約非常麻煩。
因為他們只要取得自己需要的東西之後，就會開始耍賴。這是我非常理解的。所以說，進行情報交流是沒有問題的，但是像這次這樣的情況，無論如何都不會選擇紋章教徒的相關者。
“不，不。一點也不尋常啊，路易斯哥哥。雖說是合約，但為何要做到拼上自己的性命的程度啊！？”
在這一點上，伍德和塞雷爾，他們很合適。

因為他們擁有某種程度的人望，而且非常需要的保護。在只能依靠自己的力量生存作為根本規矩的貧民窟裡，這樣的人真的很少見。

作為哥哥的伍德是個勇敢的男人。在雙親去世，沒有可依賴的人的情況下，憑借巨大的身軀帶大了塞雷爾。即便是在貧民窟這樣惡劣的環境中，性格也沒有絲毫扭曲的剛毅的男人。

他們本來應該是不需要保護的。
伍德因為身材高大，在貧民窟的年輕人中成為了中心人物，也因此被衛兵團盯上，在保護著妹妹之後被打的半死半生。
“好吧。我有我的想法和做法。這不會因為任何人而發生改變。”
手指不由自主地尋找口嚼煙，又在咽下去前即使停止。真是危險，我又不是被虐主義者，不想再受痛苦的折磨了。
向貧民窟居民施以暴行。
在貧民窟裡，居民不斷被衛兵折磨，對他們來說稱之為日常也不為過。

絲毫不在意法律，只為自己解悶，而對貧民窟的居民加以責難。像那樣的傢伙，當然也是存在的。
「……」
塞雷爾垂下眼睛，撫摸著手裡拿著的舊喇叭。

是的，一切都是日常。但是，對於伍德和塞雷亞來說不是如此。後來他們的日常生活發生了很大的變化。衛兵團奪走了伍德的勇敢，塞雷亞的聲音。如今，伍德已經無法靠近紛爭，而塞雷亞，她手中的喇叭就是她的聲音。

無論是多麼不講道理的事，無論是多麼平等的事情，全部都是這個世界的日常。

世界上，不會有留意這對兄妹發生的事情的傢伙吧。他們的日常生活被惡魔的心血來潮而奪走，被當作破爛一樣對待，世界也只會照常流轉。啊啊啊，真是太棒了。

大教堂所倡導的靈魂平等的產物就是這個，我想到這，肚子變得更加絞痛。
“沒必要在意。你們不也為我們介紹了住所麼。”
芙拉朵在邊上幫腔。

她終於捨得從沒有月光的陰影中走出來了，瞳孔下生出了黑眼圈，而且還有點腫。藏起來應該就是不想讓我看見那雙眼吧，這就是所謂的“女人心”吧。嗯嗯，還是裝作沒看見比較好。

芙拉朵代替我繼續與兄妹倆交談。
“倒不如說，就算把路易斯放任不管他也會受傷。請不要過於介意。”
這傢伙是因為我躺著不能動，就在這說我壞話吧。
雖然臉頰微微抽搐，但那也是在對伍德和塞雷爾進行安慰，所以並沒有說出口。

而且啊，即使被問到為什麼要拼上性命，也不可能回答啊。怎麼可能說得出口呢？
在曾經的救世之旅中，所有人都只對除我之外的成員進行送禮、聲援，只有你們兄妹對我懷有敬意，我到現在都難以忘記這件事，而且還說那這是你們唯一的救贖。

眼皮有點沉了，果然受傷的身體需要休息。看到芙拉朵停止了講話，我就張開嘴巴。
“因為我有一段時間都將處於這種狀態，所以塞雷亞小姐這段時間請不要出門。希望可以讓我靜養一下……”
“——不，那是不可能的。”
那個聲音，出現的非常突兀。而聲音的出處，是開著的門側，那裡有一道身影。

在月光的照耀下，影子的輪廓一點一點地露出來。那是在小小的身體上背負大桶，臉上浮現出微弱笑容的拉爾格·安的身姿。

騙人的吧，嘴巴喃喃著，但她位於此地的事實已經是最好的說明。
她來到這裡，只會有一個原因。也就是說，
“我來此轉述聖女瑪蒂婭的傳言……滿月之夜，也就是今天，將會吹響第一次進攻的號角。”
災難即將降臨。