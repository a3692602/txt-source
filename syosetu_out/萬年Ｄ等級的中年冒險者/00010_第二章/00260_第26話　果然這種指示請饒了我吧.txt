我名字叫瑪莉莎。
熟人用「瑪莉」這個愛稱叫我。

母親是在王宮任職的學者，正好比現任国王費爾納澤三世陛下的愛女──菲奧拉王女殿下的誕辰早上半個月生下了我，所以被任命為乳母。

我和菲奧拉殿下喝著同樣的奶，被形同姐妹般撫養長大。
像她這樣年紀，侍候在她身邊的騎士，基本都是這樣的經歷。

順便一提，下級貴族出身的母親，在成為乳母之時候被授予子爵之位。

因此以身份地位來說，我只是子爵家的女兒而已。

菲奧拉殿下從小就很可愛，但最近更長得越來越漂亮了。
如今人氣度更已有凌駕国王陛下之勢頭。

優秀的當然不只是容貌方面。

不論心也純潔，就連學問方面也表現為非凡之才。

那樣的她，對此我當然深表敬愛。
可以為她粉身碎骨也在所不惜。

因此只要這是她的命令，我願意使盡一切手段去執行。

⋯⋯雖然早已覺悟，可是啊⋯⋯

「果然這種命令，無論如何請饒了我吧⋯⋯殿下⋯⋯」

一邊偷偷跟在某個同學的後面，一邊不由得抱怨起來。

因為，就是要＜把他的毛巾弄到手＞啊！
而且在實技課程上已被大量汗水滲入濡濕！
把那種東西弄到手之後，你到底打算幹嘛啊？殿下！

雖然容姿端麗、頭腦清晰，可惜這樣的菲奧拉殿下有那麼一點殘念。

那就是，變態⋯⋯

不，那個以上逾越就是對王族的不敬了。
即使是在心裡，也應該控制這種想法吧。

咳哄～。
不管怎樣，也可能天真爛漫的性格，造成菲奧拉殿下是對自己的願望非常坦率。

就是在那個少年進入這個學院，不久以後發生的事情。
由於在迷宮中被魔物群團團圍住，在陷入困境之時得到救助作為契機。

不過，我認為他的幫忙並非十分必要。
話雖如此，一旦陷入了那種狀況的時間點，作為一個陪侍騎士來講，可以認為是失格了。

面對他英姿颯爽的現身解決了危機的模樣，殿下或許想起從小便憧憬的童話了吧！
雖然是被魔物俘獲的公主殿下，被勇猛果敢的騎士救出，一切如同物語般貼切展開，但是殿下從很早以前就喜歡那樣的故事⋯⋯

讓殿下狂熱到那種程度，這邊理所當然會產生嫉妒呢。嘛～那是情緒問題，控制不住也是沒辦法啊！
但是啊，果然還是不能把他拉進隊伍成員中。

因為他是平民，所以想取得殿下的信任⋯⋯但老實說，最怕這只不過是理由的一部分。

真正的理由，卻是為・了謀・劃・殿・下・的・性・命，因此只有確信可靠的人才能作為隊伍成員進入，就是這樣的。

「⋯⋯就是這樣，除了那個之外，（我）都想盡可能的滿足您的要求⋯⋯」

⋯⋯這個難度也太高了吧！

試試看直接點問＜那個毛巾，能給我嗎？＞是否比較好呢？
只是被問到＜但是，有什麼用呢？＞的話，究竟怎麼回答才好呢？

當然決不能報上殿下之名。

那樣的話，＜我想要＞只能是這麼表達了吧。

──如果有了沾滿珂露雪大人汗味的毛巾，就可以更安穩入睡了。

確實是為了殿下我甚至願意獻出一切。
不過，如果可能的話，我真不想用那種方式來展示忠誠⋯⋯

即便如此，我也做好了覺悟，邁出了一步，正在那一刻。

＜飄啊 ～飛啊～飄啊⋯⋯＞這樣。

他肩膀上搭著的毛巾，滑落到地面上。

而且他沒有注意到那個，就這樣走了。

啊～啊～，女神大人像是一直在保佑我似的。
這是絶佳的機會。

現在的話，不必沾污自己的名譽同時也能完成任務。

我消除了氣息，然後迅速地移動接近落在地上的毛巾。
沒關係，他還沒有注意到。

然後快速回收毛巾，放入準備好的密閉容器內，立即轉生折返離開──

「嘎嗚～！嘎嗚～！」
「⋯⋯？」

剛剛，好像能聽到什麼，是狗叫聲嗎？

但是，這種地方不可能有狗。
是幻聽吧？

我以全速衝刺，迅速離開現場。


◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇◇

「喂，不行啊，玖！」
「嘎嗚～！嘎嗚～！嘎嗚～！」

突然在影中的玖突然吠起來，珂露雪立刻慌了手腳。

環顧四周，幸虧四下無人。
對此鬆了一口氣，向小玖詢問。

「你怎麼了，突然？」
「汪嗚！汪嗚！⋯⋯汪嗚！」
「⋯⋯嗚呣！」

雖然珂露雪能簡單理解玖的意思，但如果內容很複雜的話，果然還是難以辦到的。

「對不起吶，你想表達的，我不太清楚」珂露雪說著，小玖它傷感地叫一聲「Kuu～～」
「咦？」

於是，珂露雪才發現搭在肩上的毛巾不見了。

「是在哪裡弄丟了嗎？」
「汪嗚！汪嗚！」
「啊，你該不會是想告訴我這個吧？」

然後從來路折返（尋找），不過⋯⋯

「汪嗚！」
「誒？怎麼這邊？」

在玖的帶領下，珂露雪向另一條路走去。

我明明沒來過這邊，為什麼呢⋯⋯這麼想著，不久總算來到目的地，前方那是⋯⋯

「王女大人？還有，身邊陪伴著的⋯⋯確實，是叫瑪莉莎小姐什麼的來著？」

並且珂露雪還看到了，看到瑪莉正把什麼交給公主，那是──

「⋯⋯回，回去吧，玖。只不過是一條毛巾而已，丟了也沒關係⋯⋯」

──我什麼也沒看見。