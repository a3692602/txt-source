蘿茲用她那蜂蜜色的眼眸觀察著黑衣男子們。
自她被帶到大講堂後，已經過了不少時間。太陽已然西下，吊燈那溫暖的光芒照亮著講堂。
綁著手腕的拘束具已經用藏著的匕首割開了。蘿茲裝作被拘束的樣子坐在椅子上、將匕首偷偷傳給了坐在身旁的學生會成員，而現在應該已經依次傳給其他學生了吧。
隨時都能夠行動。但她很清楚、即使那樣做也不過是徒勞之舉罷了。
雖然黑衣男人的數量並不算多，但每一個都是不可輕視的實力者。而且他們都處在統一的指揮下。其中那個被叫做雷克斯的男人、和大概是其長官的『瘦騎士』的力量更是鶴立雞群。沒能正確把握敵方實力、便貿然反抗的教師，最後什麼也沒做到就被慘殺了。即使能夠使用魔力，也不清楚到底有幾分勝算。
幸運的是雷克斯已經有一段時間沒有現身了。如果是被外面的騎士團打倒了就太好了⋯⋯可又不覺得雷克斯那樣的實力者會有所大意。必須得趕在雷克斯回來前想辦法做些什麼才是，老實說這是蘿茲現在最迫切的想法。
雖然『瘦騎士』大多時候都呆在深處的房間，但時不時也會在大講堂現身，並對雷克斯還沒有回來一事怨聲載道的罵幾句。從『瘦騎士』那濃密的魔力和站姿來看，其實力恐怕已經超越了達人的領域。甚至凌駕於那個愛麗絲・米德嘉爾之上⋯⋯雖然不太願意這麼想。可如果真是那樣的話，即使取回魔力蘿茲能贏過『瘦騎士』的可能性也非常低吧。
不管怎麼說、現在還不是行動的時候。然而、時間不多了也是事實。
隨著時間的經過、能感覺魔力正不斷地從體內流失。這應該與不能使用魔力的現象有所聯繫吧，但正確的理由不得而知。雖然蘿茲還比較從容，但魔力量較少的學生已經出現不適反應了。再過幾個小時的話，恐怕就會有學生出現魔力欠缺症了吧。這樣一來反擊的機會便再也不會來了吧。
心中涌上的不安、焦躁
而將其抑制住的、無論何時都是那名少年的身影。
每當想起那挺身保護自己的雄姿，蘿茲的胸口總會涌上一陣熱流。決不能讓他的心意付諸東流。不知第幾次的這麼念想著，蘿茲等待著時機的到來。
而現在、那個瞬間突如其來地到來了。
突如期然的、大講堂被炫目的白光所照亮。
並不知道那是什麼。可比起想法蘿茲的身體更早動了起來。
無論那光是什麼都好。只是、這就是最後的機會，自己的本能正這麼訴說著。
趁著所有人都被炫光奪走了視野的瞬間，蘿茲眯起眼睛衝向了離自己最近的黑衣男。
就在將手伸向那破綻百出的脖子的瞬間，蘿茲注意到了。
能使用魔力了！
蘿茲的手刀瞬間斬斷了男人的脖子。
她並不知道為什麼能夠使用魔力。但那才真的是怎樣都好的事。蘿茲只是從失去腦袋的男人腰間奪走劍，並將之高舉吶喊道。

「魔力已被解放！！站起來、此刻正是反擊之時！！
整個大講堂都為之沸騰。
學生會的少女開始行動，瞬時間便掙斷拘束，重獲自由的學生們也紛紛動了起來。所有人的意識化為一體，那份狂熱震撼了空氣。
蘿茲解放了那龐大的魔力，僅憑一擊便將黑衣男人打飛了出去。
為的、只是勝利。
蘿茲清楚的認識到、自己在這個瞬間成為了反擊的象徵。
只要蘿茲仍在戰鬥，大家也會隨之跟上。為此、她必須不斷展現出任誰都承認的勝利不可。
不顧魔力的分配，蘿茲全力揮舞著手中的劍。

『緊跟學生會長！！』
『奪走他們的劍！！』

以一己之身承受著注目、敵意、與喝彩，斬殺了眾多敵人、解放了諸多學生，持續不斷地戰鬥著。
所有人都心懷憧憬、追逐著那身姿。
然而、這不過只是無視魔力分配的無謀衝鋒罷了。就算魔力量再怎麼龐大，不斷被抽取著魔力的蘿茲已經離極限非常近了。
她在感受著現狀的同時，冷靜的估算著自己的極限。不斷流逝的魔力、變得遲緩的劍軌、以及越來越沉的身體。
本能一擊擊倒的敵人現在需要兩擊。又從兩擊變成三擊。
還有一點、還差一點⋯⋯而與其想法背道而馳，不知何時蘿茲被包圍了起來。
再來一個人。自己便將迎來極限吧。
現在大講堂充斥著學生們的狂熱。即使蘿茲在這裡倒下，他們也已經停不下來了。
一名少年的心意由蘿茲、而蘿茲的心意則由大家來繼承。縱使其過程伴隨著生命的凋零，但其心意確確實實地被傳遞了下去。
並不是無用功。
無論是少年的死，還是即將到來的自己的死。
藝術之国的蘿茲走上劍之道是有著確實理由的。那是從沒有對任何人說過的、真的非常無聊的孩子的夢想。但是、蘿茲是認真在追逐它的。現在的自己、距離那夢想是否又近了一步呢。
邊想著那樣的事，蘿茲揮下了她最後的一劍。
那一擊中幾乎沒有包含什麼魔力。甚至連力量也沒有。速度也不是很快。
然而那卻比至今為止的任何一擊都要美麗、斬下了敵人的首級。
那是蘿茲人生中手感最好的一擊。那個瞬間、蘿茲仿彿抓住了某種重要的感覺。
她只是⋯⋯

為即將走到盡頭的人生、感到不甘。注視著從四方揮下的利刃，蘿茲不禁祈願著希望能夠再多活一天。
然後。
那個願望、實現了。
漆黑的旋風席捲而過。
那揮灑著鮮血、霎時間便將周圍的敵人一掃而空。
就像是時間停止了一般，周圍重歸寂靜。
而其中心、站著一名身著漆黑長大衣的男人。

『漂亮、揮舞著美麗劍技之人啊⋯⋯』

面向蘿茲、他用仿彿從深淵響起的聲音說道。
那應該是稱讚蘿茲方才劍技的話語吧。但是、蘿茲卻受到了難以用言語表達的衝擊。

『吾乃Shadow』

自稱Shadow的那個男人、他的劍法⋯⋯就是、如此的凌厲。

「我、我是蘿茲。蘿茲・奧利雅納⋯⋯」

從衝擊中緩過神來，蘿茲用顫抖的聲音答道。
Shadow的劍法位處於遙不可及的高處。那是融合了諸多技術、幾經淘汰研磨、再加上絶不鬆懈的修行才能達到的境界。蘿茲能從中感受到悠久的時光。那是蘿茲至今都從未見過的、完成型的劍法。

「來吧⋯⋯我忠實的部下啊⋯⋯」

Shadow朝天放出了青紫色的魔力。沐浴著那光芒、身著漆黑裝束的集團突入了大講堂。
難道、是增員⋯⋯？

蘿茲的不安以杞憂而結束了。
黑色裝束的集團華麗地落地、隨即便於黑衣男們展開了戰鬥。
內部分裂⋯⋯好像也不是那麼回事。看著也不像是騎士團的人。
凝神觀察便能發現黑裝束集團全都是由女性組成的。然後。

「好強⋯⋯」

無論是誰都很強。只是純粹的很強。
黑衣男人的數量瞬間就減少了。
她們的劍法都與Shadow一樣。而率領著這般猛者的就是Shadow。

「Shadow大人、您沒事就好」
「紐嗎」

身著黑裝束的女性跪在了Shadow身旁。

「首謀者正企圖在學園放火逃亡」
「愚昧⋯⋯此處就交給你們了」
「是」
「真以為自己逃得掉嗎⋯⋯？」

Shadow低聲嗤笑著。隨即掀起大衣衣擺，僅憑一刀便斬開了講堂的大門。在近處的黑衣男人也一同化作了肉塊。
那看上去就像是在模仿蘿茲的劍術一樣。他仿彿像是刻意展示般的橫掃而過，然後就那樣悠然自得地消失在了暗夜中。
他的所有動作，對蘿茲來說都是最棒的範本。

「沒事嗎」

被稱作紐的女性，向蘿茲問道。

「是的⋯⋯」
「真是出色的劍法」

她這麼說著，便架起漆黑的長刀加入了戰列。
然而、紐的劍術也絶非什麼尋常之物。眼看著黑衣男人被單方面的砍倒在地。
蘿茲感覺自己的常識，不、是魔劍士的常識正在崩壊。黑裝束集團所展現出的劍法，不所屬於任何一種既存的流派。
那是一種全新的流派。
這般流派、這般集團、究竟是從哪裡冒出來的。至今為止居然毫無耳聞簡直不可思議。

「是火、著火了！！」

那聲音讓蘿茲回過了神來。順著聲音看去，只見大講堂的深處竄出了火舌。

「離出口近的的人先撤離！！」

蘿茲大喊著指引學生們。多虧了黑裝束集團，犧牲者的數量並不多。戰鬥正迎來尾聲。
蘿茲擔著傷員將他們送到出口。

「騎士團來了！！」

那道聲音讓所有人心中的石頭仿彿都落了地。蘿茲也差點因脫力而倒下，還好她趕忙重整了意識。
不斷有學生被從大講堂中救出。隨著火勢逐漸變強，黑衣男人也全滅了。
然後、不知何時黑裝束的女性們也消失了。
簡直如同最初就不存在一樣、沒有留下任何痕跡、不被任何人注意到，無聲無息地隱去了踪跡。
直到最後都忙著救助學生的蘿茲，回頭看向燃燒著的大講堂。

「他們究竟⋯⋯」


◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇

夜晚的副學院長室被遠處的火光染的微紅。
在那昏暗的室內，一道人影正晃動著。人影從書架上將數冊書抽出來後，扔到了地板上點燃了火焰。
微弱的火苗開始吞噬書本，將房間照亮。
浮現出的人影是一名削瘦的黑衣男子。

「您打扮成這樣做什麼呢⋯⋯盧斯蘭副學園長」

削瘦的男子顫動了一下。本應只有一個人的房間內，不知何時多了一名少年。
少年坐在沙發上搭腳閱讀著書本。那是隨處可見的黑髮少年。不過、少年毫不將目光投向男子身後逐漸擴張的火焰，僅僅只是注視著厚重的書本。翻頁的聲音聽著格外響亮。

「虧你、能注意到呢」

黑衣男子這麼說到。將遮住素顏的面具取下後，露出的是一張初老男性的臉龐。將摻雜著白髮的頭髮梳成背頭的他、正是盧斯蘭副學園長。
盧斯蘭將面具扔進火中，把漆黑的裝束也脫下一併燒掉。室內變得更明亮了。

「能作為參考問問、你是怎麼發現的嗎，希德・卡盖諾君」

盧斯蘭坐到了希德的對面如此問到。

「一目了然」

希德督了一眼盧斯蘭，視線便立刻回到了書本上。

「一目了然、嗎。是走路方式，或是姿勢嗎⋯⋯不管怎麼說，真是不錯的眼光吶」

盧斯蘭看著希德，希德則看著書本。
被火焰照耀的房間裡就只有兩人的影子在晃動著。

「我也能夠作為參考問一下嗎」

希德仍舊看著書問道。盧斯蘭用沉默催促著繼續說下去。

「為什麼要做這種事。我完全看不出您是會對這種事感興趣的人」
「為什麼、嗎⋯⋯就讓我們聊聊過去的事吧」

雙手抱臂、盧斯蘭小聲的低語道。

「過去的我曾經立於頂點之上。這是在你出生之前的事」
「我聽說您曾在武神祭上取得過優勝」
「武神祭什麼的，與頂點相去甚遠。真正的頂點是在那遙遠的前方的東西。雖然這麼對你說你也不明白吧」

盧斯蘭笑了。那並非是嘲弄，而是仿彿已經疲倦了一般的笑容。

「我在立於頂點後馬上就患病了呢、從前線退了下來。煞費苦心總算到手的榮光一瞬間就結束了。在那之後我不斷地尋求著能治好疾病的方法，然後在露克蕾婭這位古遺物研究員身上看到了可能性」
「話會很長嗎？」
「稍微有點吧。露克蕾婭是雪莉的母親。因為過於聰明而被學術界排斥的女人。但是作為研究者卻有著最高峰的知識，她的立場對我對我來說正好合適。我支援了她的研究，收集了大量的古遺物。露克蕾婭專注於研究，而我則是利用那份研究。因為她對財富和榮譽都沒有興趣，我們構築起了不錯的關係。然後我與『強欲之瞳』相遇了。那正是我所尋求的古遺物。然而、露克蕾婭⋯⋯那個愚蠢的女人竟然說『強欲之瞳』非常危險，想要提出讓国家來管理的申請。所以我殺了她。從身體的末端向著中心刺擊，最後貫穿心臟然後扭了一下」

希德沒有合起書、就那樣閉起眼聽著盧斯蘭的話。

「『強欲之瞳』雖然留在了我的手上但還在研究途中。但是我立刻就遇到了正合適的研究者。那就是露克蕾婭的女兒、雪莉。她什麼都不知道，什麼都沒有懷疑，為了我盡心盡力。全然不知我是仇人呢。真是非常可愛、又愚蠢的女兒啊。托她們母女二人的福，『強欲之瞳』才得以完成。之後只要安排一個收集魔力的舞台，然後準備一個剛剛好的偽裝就行。今天⋯⋯是實現我願望的最棒的一天」

庫庫庫地盧斯蘭嘲笑著。

「如何、能從中參考到什麼嗎」

對盧斯蘭的疑問，希德睜開了眼睛。

「大致上都明白了。只不過⋯⋯我有一個在意的問題呢」
「說說看」
「殺死了雪莉的母親，並利用了她的事是真的嗎？」

目光從書本上離開，希德注視著盧斯蘭。

「當然是真的。生氣了嗎，希德君」
「誰知道呢⋯⋯我總是明確的劃分開對自己來說重要的東西，和並非如此的東西」

希德微微伏下目光。

「能問問、為什麼嗎？」
「為了心無旁騖、吧。我有著無論如何都想達成的目標，而那在非常遙遠的地方。所以我才會進行削減」
「削減了？」
「大家在活著的過程中重要的事物都會逐漸增加。交到朋友，交到戀人，獲得工作⋯⋯這樣逐漸地增加。但是我卻反過來削減掉。這也不需要，那也不需要。在像這樣捨棄掉各種各樣的東西之後，剩下的就是無論如何都無法捨棄的東西了。而我則是為剩下的這極少數的東西而活，所以除此之外的東西老實說怎麼樣都好」

希德合上書本。然後站了起來，將書投入了火中。

「也就是說愚蠢的母女怎麼樣都無所謂的意思嗎」
「不。雖然我確實說了怎麼樣都好，但也並非完全如此呢。我現在稍微有些⋯⋯不愉快呢」

拔出了腰間的劍，希德如此說道。

「差不多開始吧。再磨蹭下去的話感覺會有人來礙事」
「也是呢。很遺憾離別的時刻到了」

盧斯蘭也起身拔出了劍。
兩柄白色的利刃被火炎照耀的熠熠生輝，勝負只在一瞬之間。
盧斯蘭的劍撕裂了希德的胸口、鮮血四濺。
希德的身體就那樣摔破門扉，被投向了火勢凶猛的走廊。紅蓮之焰瞬間就包覆住少年的身體、隱去了其身影。

「再見了、少年」

盧斯蘭收起了劍。走廊的火焰涌入了室內，燃得更加旺盛。就在盧斯蘭轉身準備離去的瞬間。

「你想去哪兒」
「⋯⋯！」

從背後傳來的是宛如從深淵響起的低聲。盧斯蘭回過頭，在那裡的是一名漆黑的男子。用奇術師的面具隱藏臉部，兜帽拉的很低，身穿漆黑的長大衣，而長大衣正熊熊燃燒著。但男子卻仿彿毫不在意一般、拔出了漆黑的長刀。

「你這傢伙是⋯⋯！」

盧斯蘭架起了劍。

「吾乃Shadow。潛伏於影、狩獵陰影之人⋯⋯」
「你就是Shadow⋯⋯」

架起白色利刃的盧斯蘭，和垂下漆黑長刀的Shadow就那樣相對而立著。
兩人在相互對視了一會兒後，盧斯蘭率先拉開了距離。

「原來如此、很強嘛」
「嚯⋯⋯」
「我也是以劍為生之人。只要對峙便能明白大部分的事。就連現在的我非常不利也是吶。讓我拿出全力吧」

盧斯蘭吞下了從懷中取出的紅色藥片。接著將『強欲之瞳』和其制御裝置拿了出來。

「『強欲之瞳』的真正價值只有將兩者合在一起才能得以發揮。就像這樣」

喀嚓地一聲，兩個古遺物組合在了一起。
緊接著、兩個古遺物發出了耀眼的光輝，發著白色光芒的古代文字從古遺物的中心逐漸展開。房間裡古代文字的螺旋舞動著，盧斯蘭嗤笑著將古遺物按在了自己的胸口上。

「此時此刻、我將獲得重生」

古遺物逐漸埋入了盧斯蘭的胸口。
仿彿落入水中一般，穿過了衣服和肌膚。

「哦哦哦哦哦哦哦哦哦哦哦哦哦！！」

盧斯蘭抓撓著胸口發出了咆哮。
古代文字之光向著盧斯蘭集中，不斷地刻入他的身體。格外耀眼的光芒將房間染成了一片純白。
接著。
在光芒收束之後，在那裡的是單膝跪地的盧斯蘭。
一邊冒著白煙盧斯蘭一邊慢慢地站了起來。面向前方的臉上，仿彿刺青一般刻著微微發光的文字。

「太美妙了⋯⋯真是太美妙了⋯⋯力量回來了，病痛也治癒了⋯⋯！」

以盧斯蘭為中心呼嘯的魔力晃動著火焰。
仔細一看不僅僅是臉上，就連脖子和手之類的地方也刻滿了文字。

「明白嗎，這高騰的力量！這遠超人類界限的魔力！」

接著、盧斯蘭露出了嗤笑。

「首先就用你這傢伙來試試手吧」

盧斯蘭的身影消失了。
下一個瞬間、盧斯蘭出現在了Shadow的背後，揮下了劍。
尖銳的聲音回響四周，以兩人為中心大氣顫動了。

「嚯、虧你能防住呢」

定睛一看，Shadow仍然背向著後方，漆黑的刀刃擋下了盧斯蘭的劍。
雖然盧斯蘭試著將劍強行壓過去，但是漆黑的刀刃卻一動也不動。

「看來稍稍有點小看你了啊。那麼這樣如何」

盧斯蘭的身影再一次消失了。
這次連續發出了數次尖銳的響聲。
一次、兩次、三次。
每當聲音響起，Shadow的劍便微微地移動。僅僅是非常些微的、最小限度的動作。
當聲音第四次響起之後，盧斯蘭出現在了Shadow的面前。

「沒想到連這也能防住嗎。不得不承認，你非常的強」

然後以游刃有餘的笑容注視著Shadow。

「為向那份強大表達敬意，我也拿出真本事吧」

盧斯蘭的架勢改變了。
劍舉到了上段，龐大的魔力向那裡聚集。劍發出了白色的光輝，產生了魔力的旋渦。

「就在那個世界自豪讓我使出了真實實力吧」

那一擊、以極為驚人的威力和速度襲向了Shadow。
然而。
漆黑的刀刃就連那也輕鬆擋住了。

「什麼！」

漆黑的刀刃與光之劍碰撞出火花。

「就連這也能承受得住嗎！」
「難不成⋯⋯就只有這種程度嗎？」

在極近距離下、兩人互相瞪視著。

「咕⋯⋯還沒完，現在才剛剛開始呢！」

盧斯蘭的劍再一次加速。
白色的殘像在空中劃出美麗的軌跡舞動著。

「唔哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦！！」

伴隨著怒吼發出的白色劍擊，卻被漆黑的刀刃悉數彈開。

「啊啊啊啊啊啊啊啊啊啊啊啊！！」

白色的斬擊與漆黑的刀刃激突，利刃碰撞的聲音不絶於耳。
那聲音宛如一首樂曲那般，為這熊熊燃燒的夜晚增添了幾分色彩。
然而那也迎來了終結。
猛揮的漆黑之刃將盧斯蘭擊飛。他的身體撞翻桌子滾落在地。

「咕⋯⋯怎、怎麼可能⋯⋯！」

盧斯蘭捂著疼痛的身體站了起來。雖然傷口立刻回復了，但古代文字的光芒也變得暗淡。

「沒想到、居然會苦戰到這個份上。哼哼、真是了不起。但是不管你這傢伙有多強你們都已經結束了」
「結束了、是指⋯⋯？」
「哼、我們已經做好了安排，將這一連串的事件的罪魁禍首全都推給『Shadow Garden』。無論是證據、還是證言，全都已經準備好了。無論你們在戰鬥方面有多強，都無濟於事」

盧斯蘭嗤笑著，用扭曲的表情凝視著Shadow。
然而、Shadow也發出了嗤笑。面具的背後嗓子顫動，漏出了低沉的嗤笑聲。

「有什麼好奇怪的」
「覺得這樣就算結束的你簡直滑稽」
「死鴨子嘴硬呢」

收起笑容、盧斯蘭如此說到。
Shadow搖了搖頭。仿彿在說『你什麼都不懂呢』一般。

「原本我等就非行正道者、亦非行惡道者。我等所行僅乃我之道」

接著Shadow翻了一下燃燒著的大衣。

「若你做得到的話，儘管把世間罪惡全帶來即可。我等將全部背負。然而什麼也不會改變。即使如此我等也將達成我等的職責」
「即使與世界為敵也毫不畏懼嗎。這不過是傲慢啊Shadow！」
「那就試著粉碎這傲慢吧」

盧斯蘭發出咆哮的同時開始飛馳。
自上段而下的白色之劍向Shadow襲去。
而那一擊、在快要將Shadow的頭一分為二的瞬間錯開了。

「什麼！」

鮮血飛舞。
盧斯蘭的右腕部，被漆黑的刀刃貫穿了。盧斯蘭即刻將劍換到左手向後退去。
然而。

「怎麼可能！」

這次是左腕部被漆黑的刀刃刺穿了。
Shadow的突刺不斷地襲向後退中的盧斯蘭。

「咕⋯⋯嘎⋯⋯！」

在連肉眼都無法捕捉的高速突刺面前，盧斯蘭毫無還手之力，渾身逐漸被血染紅。
腕部，腳尖，上臂，大腿，無數的突刺刺向了盧斯蘭。
接著逐漸向身體的中心集中。

「從身體的末端向著中心刺擊⋯⋯」

在突刺的間隔中響起了Shadow低沉的嗓音。

「最後貫穿心臟然後扭了一下⋯⋯來著？」

與話音一同，盧斯蘭的胸部被漆黑的刀刃貫穿了。

「什⋯⋯！！」

盧斯蘭一邊口吐鮮血的同時，仍抓著貫通胸口的漆黑刀刃進行著抵抗。
盧斯蘭的視線，和面具背後的少年的視線對上了。

「你這傢伙、難道是希⋯⋯！」

正當盧斯蘭想要說出什麼的瞬間，漆黑的刀刃擰了一下。

「嘎⋯⋯啊嘎⋯⋯啊啊⋯⋯！」

接著在漆黑的刀刃拔出後，大量的血液流了下來。盧斯蘭眼裡的光芒和古代文字的光芒，都漸漸地消失了。最後留下的、就只是一具消瘦的初老男性的屍體。
就在那時──傳來了小小的腳步聲。

「義父大人⋯⋯？」

被回濺的鮮血淋滿全身的Shadow回過頭後，出現在那裡的⋯⋯是粉色頭髮的少女。

「義父大人！！！！！！」

粉色頭髮的少女從Shadow的身旁跑過，依偎在盧斯蘭的屍體旁邊。

「不要啊啊⋯⋯⋯⋯義父大人，為什麼⋯⋯為什麼⋯⋯！！」

雖然雪莉抱著消瘦的屍體流下了淚水，但義父的身體已經再也不會動了。
少女的眼淚濡濕了盧斯蘭的面部。俯視著那淚珠的Shadow背過了身去。

「你什麼也不需要知道⋯⋯⋯」

然後在少女悲切的哭泣聲，Shadow的身影消失在了紅蓮之焰的深處。

（插圖０１２）


◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇

背部受了重傷的少年似乎在校舍內被保護起來了。
在聽說這個消息後，蘿茲急忙趕向了救護帳篷。
黑夜中校舍不斷地燃燒著。
空下手的老師和學生們齊心協力做著水桶接力。
騎士團則為了救助傷者和追踪『Shadow Garden』而展開了行動。
穿過匆忙的人群，蘿茲終於來到了救護帳篷前。
被保護的少年是黑髮的魔劍士學園的一年級學生、特徵和他一模一樣。
不過那個時候、他應該已經死了才對。
可也沒有仔細確認過他的死亡。
沒有這樣的餘裕和時間。
所以、也許，他說不定還活著。
也許他就在這個帳篷裡面也說不定。
蘿茲無法割捨那一份微小的希望。
就算理性一直在否定著自己的想法，但內心卻依舊懷揣著希望。
蘿茲察覺到了自己心中的弱小。
帳篷內充斥著血與酒精的異味，救護班不斷跑動地救助著患者。
蘿茲一邊確認著每個人的臉龐，一邊向帳篷深處走去。
然後、她發現了一名黑髮的少年。
他安靜的趴在床上接受著背部治療，與醫生進行著問答。
看來還有意識。

「那、那個⋯⋯是希德・卡盖諾君嗎？」

蘿茲抱著抓住救命稻草的心情問道。

「是哦⋯⋯？」

少年轉向了這邊，那張臉、確實是那時的勇敢少年。

「太好了⋯⋯真的太好了⋯⋯」
「誒⋯⋯等等！？」

回過神來，蘿茲已經抱住了希德。
緊緊地把他掙扎著的腦袋抱在胸口，祈禱著不要再度失去他。
蘿茲的心裡涌起一股暖流。

「那、那個，我們還在治療中⋯⋯」
「哈！是啊」

醫生那略顯困惑的聲音，讓蘿茲回過神來放開了希德。

「那個、希德的傷勢怎麼樣了？」
「背部傷的相當嚴重呢，不過奇跡的是神經和內臟都沒有受損，不會有生命危險」
「嘛！真的嗎！？」
「是的、千真萬確」
「太好了！太好了！」

蘿茲的身體微微地顫抖著表現出內心的喜悅。

「那個、是這樣的，我好像是在無意識裡避開了致命傷。不不不、因為我失去意識了所以我什麼都不知道，感覺大概就是這樣我才奇跡般地生還的。是真的哦」

不知為何、希德看上去就像是一副在找藉口的樣子。

「平日的鍛鍊在突發狀況時起到了作用，真是美妙的事啊」
「不、和那個有點不一樣」

蘿茲單膝跪地與希德的視線交合在一起。

「不、一定是這樣的。正是因為你那堅持不懈的努力與火熱的信念，才能引發這樣的奇跡」

蘿茲撫摸著希德側臉，以呼吸都能觸及到的距離仔細看著他。

「那個⋯⋯」
「不用說出來，你內心的想法我確實收到了」

蘿茲濕潤的瞳孔緊緊地盯著希德的眼睛。
她的雙頰染上了宛如薔薇般的緋紅。

「如果你能接受這奇跡的話，我倒是沒什麼意見。以後可別說什麼這不正常之類的話哦」
「好的。可現在還得請你好好休息呢」
「交涉成立，那麼晚安」

蘿茲溫柔地注視著那閉上眼睛的少年。她的心中響起了的迄今從未有過的高鳴。
這份心情、蘿茲本來僅僅只是作為知識而知道的。
但現在、她終於明白了。

「被你所救的這條性命⋯⋯既然如此、我就向你獻上我的心⋯⋯」

蘿茲撫摸著希德的頭髮。
然後在入睡的他的身旁迎來了黎明。


◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇

「不覺得做的還挺像的嗎？」

這麼說著遞出傳單的、是美到讓人驚艷的金髮精靈。她身著宛如暗夜般的漆黑禮裙，于深夜造訪了三越商會。
收下了美貌的精靈遞過來的傳單，伽瑪含糊其辭道。

「阿爾法大人⋯⋯那個、我不好說什麼⋯⋯」
「抱歉、有點難以回答吧」

被叫做阿爾法的美貌精靈呼呼呼地笑了。阿爾法所拿出的是通緝令。在那上面描繪著身纏漆黑長衣的Shadow的身姿。

「王国的仇敵、Shadow。無差別殺人、監禁、放火、強盜⋯⋯何等罪大惡極之人啊」
「『Shadow Garden』的通緝令上也有阿爾法大人的名字。雖然只有名字就是了」
「讓我看看」

阿爾法閱覽起了伽瑪拿出的另一張通緝令。

「『Shadow Garden』⋯⋯這也是罪孽深重的組織呢」

暖爐的光亮映照著她的側臉，超脫現實的美貌浮現於夜晚的黑暗之中。

「但是真是遺憾呢。明明都趕著回來了，結果到的時候基本都已經收拾乾淨了什麼的」

阿爾法用暖爐的火點燃了通緝令。就那樣觀望著從末端開始逐漸變成黑色的紙張低語道。

「就讓我等背負世間一切罪惡吧。然而什麼都不會改變。即使如此我等也將達成我等的職責。真是不錯的話呢⋯⋯」

在阿爾法視線的前方，通緝令逐漸化為飛灰落了下去。

「在心中的某處、我還認為自己是站在正義的立場上的呢。但看來他並非如此呢」

被搖曳的火焰照耀的美貌上陰影不斷變換著，其表情帶來了不同的印象。時而宛若女神，時而又好似惡魔。搖曳的火焰反覆無常地改變著那姿態。

「我們也不得不回應他的覺悟」

看到回過頭來的阿爾法的表情，伽瑪不禁咽了一口氣。

「把手頭有空的『七陰』全部召集起來」
「遵命、立刻照辦」

伽瑪垂下頭。冷汗淌過她的脖頸，消失在了胸口的谷間。
伴隨著一陣微涼的晚風，伽瑪抬起了頭，然而那裡早已空無一人。
只留下激烈地搖晃著的暖爐火焰。


◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆

「那個⋯⋯！」

在被燒的半毀的學園前，被搭話的平凡的黑髮少年回過了頭。

「啊啊、抱歉抱歉，稍微發了一下呆。怎麼了嗎？」
「因為聽說在這裡等著的話就能見到你。我有想要告訴你的事⋯⋯」

粉发的少女看著少年說道。

「到接受尋問為止還有一段時間所以沒關係啦。反正課也會暫時停講」
「那個、前幾天真是非常感謝」

桃髮的少女低下了頭。

「拖希德君的福，真是幫了大忙了」
「並不是什麼大不了的事啦」
「要是只有一個人的話，我什麼也做不到」
「沒關係，不用放在心上」
「然後、今天還有其他想要報告的事。那個、我決定去留學了」
「啊啊、所以才帶著那些行李」

粉发的少女手上拿著大量的行李。

「嗯。現在要去坐馬車。到拉瓦伽斯去」
「學術都市嗎⋯⋯真厲害呢」
「我、找到了不得不做的事。可只靠現在的知識是不夠的」
「是嗎、加油哦」
「而且⋯⋯已經沒有留在這裡的理由了」

少女用悲切的表情回過頭看了一眼校舍。

「雖然還想和希德君多說說話⋯⋯」
「嗯。有朝一日再見吧」
「嗯、再見」

粉发的少女露出了微笑，從少年的身邊走過。

「啊、稍微等一下」
「怎麼了嗎？」

被少年搭話的少女回過了頭。

「可以問問、不得不做的事是什麼嗎？」

對少年的疑問少女困擾地露出了微笑。

「秘密」
「是嗎」
「但是、要是一切都結束了⋯⋯能聽我傾訴嗎？」
「⋯⋯可以哦」

兩人微微地笑了。然後互相背過身去邁出了步伐。
炎熱的夏日陽光被積雨雲遮過。溫和的風運來了雨水的氣息。

「我一定會⋯⋯」

不意地、少女的呢喃聲乘著風傳到了少年的耳中。
那本該沒人聽見的低聲細語，但少年確實地聽到了。他回過頭去，注視著少女逐漸遠去的背影。
啪塔啪塔的、從天空中落下了小小的水點。將那粉色的頭髮逐漸打濕。
少年也若無其事地再度邁出了步伐。
兩人沒有再次回頭。