那正是與空中庭園這個稱呼相稱的威容。

在山的深處，開始感到呼吸困難的更深處是庭院。簡直就是把云作為立足點的都市的存在，真是不可思議的景象。
曾經被邀請到這裡的詩人，稱呼其為這世上唯一的優雅與奇異共存的城市，那就是精靈們的栖身之所。

即加薩利亞的空中庭園。
從口中自然而然地流露出感嘆的氣息。兩眉上挑，睜開了眼睛。

救世之旅的時候，這種情景也把我嚇破了膽。確實很適合用幻想來形容這座城市。人們說從前諸神還會蒞臨於大地的時候，世界一定滿是這樣的景象。

怎麼都不像我會發出的感想使得我禁不住自嘲地扭曲了表情。
強烈的風拍打著臉頰。感覺著非同尋常的寒冷。為什麼在山上即使靠近太陽也會變冷呢？真是不可思議。

「精靈王的答覆來了⋯⋯他表示非常歡迎你們。」

在馬車中欣賞著風景，品味著口嚼煙，聽見了瑪蒂婭清脆的聲音。看來是前往空中庭園的使者回來了。
即使是不高興的聲音如今聽來也覺得很溫柔啊。

「呼⋯⋯真意外。這樣的話，豈不是出乎預料的順利。老實說，我已做好了這次是白費功夫的覺悟了。」

芙拉朵發話了。

「確實很意外呢。我也和芙拉朵一樣，沒料到這就可以拜見精靈王了。」

心臟的抽搐已經停止了。但是，只要一想起那傢伙，指尖就不由得發顫。果然，很根深蒂固啊。

「不管怎麼說，既然能見面，就該去見面吧。理應如此。」

卡麗婭跳下馬車後這樣說道，銀髮晃動著。

「啊，這麼說來，該怎樣稱呼精靈王呢？」

用腳踏在大地上，一邊確認著這裡不是空中，一邊像突然想到似的瑪蒂婭發問了。

「叫芬・拉吉亞斯。聽說是叫這個。」

為了避免失禮而採取冷淡的聲調。這傢伙真是個別扭的女人啊。
芬・拉吉亞斯。芬是国王的意思，準確來說是拉吉亞斯王。

沒錯。救世之旅時，宣布協助人類，並讓艾爾蒂絲加入救世之旅一行的王的名字，確實就是這個。
就連我蠢笨的大腦，好像也記得這件事。

那麼，也許意外地沒有問題。莫名其妙的輕鬆思緒掠過我的腦海。
從過去的歷史來看，那位王和人類和諧共處這一點毋庸置疑。根據已有條件，交涉順利的話就可以結下同盟，即使不能結盟，能達成互不侵犯協定的可能性應該很高吧。

如果可能的話，也可以對周邊各国大肆宣揚我們取得了精靈的協助。
這個時代，和精靈們有聯繫的，在統治階層中也只是少部分。精靈這個字眼，對平民來說是未知，是畏懼的存在。這樣的話，只要大肆宣傳就可以引起恐慌然後降低敵軍的士氣。

沒錯，只要交涉的好，就沒有問題。把樂觀的空想和悲觀的妄想一起排出腦海，靜靜的嘆氣。

───

「聖堂騎士、加爾拉斯・加爾剛蒂亞」

阿琉珥娜は、一瞬それが自分の口から出た聲だと認識できていなかった。數瞬後、更にいうならば自ら呼び止めた相手がこちらを振り向く頃になって、ようやく自分の口が開いていることに氣付いた。

「嗯⋯⋯啊，這不是歌姫大人嗎？我，聖堂騎士加爾拉斯・加爾剛蒂亞，確實於此。」

像是開玩笑似的，把手放在胸前的樣子。他的表情和動作看上去很輕浮，很難想像他是聖堂騎士。阿琉珥娜一邊眯起那黃金色的瞳孔，一邊踢著腳。

雖然他的本領確實是聖堂騎士首屈一指的，但他的言行卻一點不像是聖堂騎士，在背地裏大家都是這麼說他的。聖堂騎士加爾拉斯・加爾剛蒂亞。當然，這種說法中確實包含著很多的妒意。

本應讓對方感到威嚴的銀製鎧甲和足具，對他來說也有些不相稱。

「歌姫什麼的，我可不是那麼叫的。嗯，不是⋯⋯嗯，那個，這樣的裝束，是要出行嗎？」

即使大腦極其的動搖，但還是吐出言語。原因是──

為什麼叫住加爾拉斯？那個原因，阿琉珥娜自己也不知道。真的，只是下意識的張開了嘴。
阿琉珥娜的話語讓加爾拉斯微微縮起肩膀張開嘴。那種動作，果然沒有一點正形。

「真是無聊的事情啊。話雖如此，你應該明白的吧，歌姫大人？」

因為你很聰明，他以輕快的語調進行補充。
看到他玩世不恭的樣子，阿琉珥娜不由得吐出一口氣。

畢竟，阿琉珥娜和加爾拉斯並不是特別親密。雖然見面的話，除了打招呼外，還可以聊聊天。但也無法稱之為熟人吧。
但是，如果說是在幾乎沒有關係親密的人的大聖堂的話，他在某種意義上來說，也可以說是非常親近的人了。不知怎麼，心底泛起一絲苦澀。

雖然加爾拉斯自己是個無法捉摸的人，但他所說的話卻是無可挑剔的。

這個時期，聖堂騎士整理好裝備在大聖堂的走廊裡走著。目的是非常明顯的。
也就是說，要前往討伐紋章教徒。
啊，是嗎。討伐有路易斯在的紋章教。阿琉珥娜眯起了眼。黃金的瞳孔中暗光閃爍。

「──這樣啊。嗯，那麼幾天後去伽羅亞瑪利亞？」

阿琉珥娜發出微微感到冰冷的聲音。風呼呼地響。
加爾拉斯無言地搖著頭，搖晃著肩膀。然後，環顧四周之後，湊近臉說道。

「不對。這是特命哦。雖然不應該外傳⋯⋯大聖堂要和精靈之長聯手了。」

那句話讓人瞠目結舌。精靈。說起精靈，就是被稱為森林之民的他們。大聖堂為什麼要做那樣的事？沒有說出那個疑問的空閑，接下來的言詞衝擊了阿琉珥娜的大腦。能聽到咽口水的聲音。

「──有那裡發來的魔術傳令。主謀魔女和那個叛徒被捉住了。」

但是，阿琉珥娜的意識遠遠地遠離了「精靈使用的魔法真是厲害啊」這樣抱怨著的加爾拉斯的話語。已經，對那樣的言詞沒有任何興趣了，那沒有任何價值。

叛徒。沒錯，會被大聖堂這樣稱呼的人，現在只有一個。在阿琉珥娜的眼裡，映出了一道身姿。是青梅竹馬，寄存手帕約定再會之人，路易斯。

是啊，為什麼沒有想到呢。我理解了。在這種還沒有做好準備的時期，聖堂騎士會前往，肯定是和紋章教徒的魔女，或者是路易斯有關係。

於是，我抱著這樣的期待，期待著如果是輕浮的加爾拉斯的話，一定能從他這打聽到什麼情報，於是發出了詢問。

阿琉珥娜的嘆息，灑落下來。空間好像在晃動。

「⋯⋯那麼，處刑在精靈的国家？」
「不，如果可以的話，我想那麼做，但是是不可能的。教皇亦下似乎希望活捉。」

因此作為聖堂騎士出差只有我一個人，真是郁悶啊，加爾拉斯說。一瞬間，眼神中失去了平時的輕浮。
原來如此，活捉。阿琉珥娜大體上理解了那個想法。

「──聖堂騎士、加爾拉斯・加爾剛蒂亞。」

浮起出和善的笑容，以優雅的言語訴說著。

「希望這段旅途順利。如果可以的話。」

小小的嘴唇，搖晃著。好像很高興的樣子，做出了那樣的表情。

「希望能讓我看看背叛者的臉。」