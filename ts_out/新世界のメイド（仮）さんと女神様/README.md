# novel

- title: 新世界のメイド（仮）さんと女神様
- title_zh: 新世界的女僕與女神
- author: あい　えうお
- illust:
- source: http://ncode.syosetu.com/n5964cj/
- cover: https://img1.mitemin.net/4t/jl/4lek1sve19h73knz1lew2tr7c73x_445_jz_s3_6dyh.jpg.580.jpg
- publisher: syosetu
- date: 2019-11-22T00:29:00+08:00
- status: 連載
- novel_status: 0x0000

## illusts


## publishers

- syosetu

## series

- name: 新世界のメイド（仮）さんと女神様

## preface


```
在我玩的MMORPG結束營運
最后一天的登录的时候，游戏中的女神大人说〔你可以使用现在使用的角色继续进行游戏〕之后，我变成了现在正在使用的角色来到了与游戏里非常相似的地方。
虽然掌握了现状，但是因为是女仆 麻里子（男主现在的角色），所以理所当然穿着理所当然的女仆服的错吗，因为各种原因顺势的成了酒店旅馆的女佣。
虽然对女仆来说有必要的技能也拥有，但主要的技能，是近身战斗和防御与回复魔法。

よくある異世界転生物っぽい話です。一話当たりの文字数は少なめですので、気楽にお読みいただければ幸いです。

プレイしていたＭＭＯＲＰＧがサービス終了になる。
最終日にログイン中、ゲームの中の女神様に「今のキャラで続編ゲーム」を勧められ了承したら、今まで操作していたキャラクターの姿でゲームとよく似た場所にいた。
現状把握を計るが、メイドスキーとして当然のメイド服姿のせいか、成り行きで宿屋兼酒場の女中さんになることに。
メイドさんに必要そうなスキルもそれなりに持ってはいるけれど、メインスキルは近接戦闘と防御・回復系魔法なのですが。

※サブタイトルに★が付いている話にはイラストがあります。
※毎週月曜・木曜午前7時を目安に更新しておりますが、都合により予告なく遅れたり休んだりする場合があります。なお、10月下旬から12月は仕事の方が繁忙につき、一応不定期更新とさせていただきます。
※あらすじと本文ではイメージが違うかもしれません。
※初投稿なのですが、書き始めてみると自分の筆が非常に遅い事に気付きました。迅速な更新は無理そうです。
※そのうち、戦闘シーン等が出てきますので、一応「R15」と「残酷な描写あり」をつけておきます。
※主人公の中身はいいかげん歳食ってますので、少々のエロなどには（多分）動じません。
```

## tags

- node-novel
- R15
- TS？
- syosetu
- おっさん向け？
- お約束
- ご都合主義
- チート？
- ネットゲーム
- ハイファンタジー
- ハイファンタジー〔ファンタジー〕
- ファンタジー
- マニアック
- メイドさん
- ロリババア
- 各自の物差しで
- 微百合？
- 恋愛（ＮＬ？）
- 昭和の薫り
- 残酷な描写あり
- 猫耳
- 異世界転移
- 異世界転移？
- 盛りババア
- 細かい
- ＧＬＢＬの判断は

# contribute

- 幻夜之雪
- EyStau
- 星宫濑奈
- 汐绚-
- Adamah
- 關注勇者的魔王
- 土之女神

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1

## syosetu

- txtdownload_id:
- series_id:
- novel_id: n5964cj

## textlayout

- allow_lf2: true

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n5964cj&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n5964cj/)
- [新世界的女仆与女神吧](http://tieba.baidu.com/f?kw=%E6%96%B0%E4%B8%96%E7%95%8C%E7%9A%84%E5%A5%B3%E4%BB%86%E4%B8%8E%E5%A5%B3%E7%A5%9E&ie=utf-8 "新世界的女仆与女神")
- http://c.tieba.baidu.com/p/6294661124
- https://twitter.com/search?q=%2523%E6%96%B0%E4%B8%96%E7%95%8C%E3%81%AE%E3%83%A1%E3%82%A4%E3%83%89%E3%82%AB%E3%83%83%E3%82%B3%E4%BB%AE%E3%81%95%E3%82%93%E3%81%A8%E5%A5%B3%E7%A5%9E%E6%A7%98
-

